@include('layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Conference &mdash; Participants Detail</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Conference</span> &mdash; Participants Detail</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <?php
    foreach ($data as $dt) {
    }
    ?>


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('layouts.sidebar')

            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Conference &mdash; Participants Detail [ID:<?= $dt->id ?>]</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div style="padding-left:20px;">
 						<a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
 					</div> -->
                    <div style="padding-left:20px;">
                        <a data-toggle="modal" href="{{url('conf/participants')}}" class="btn btn-sm btn-primary">Kembali</a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <b>Nama</b>
                                    </td>
                                    <td>
                                        <?= $dt->name ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email</b>
                                    </td>
                                    <td>
                                        <?= $dt->email ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Afiliasi</b>
                                    </td>
                                    <td>
                                        <?= $dt->affiliation ?>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <a class="btn btn-success" href="<?= asset('../storage/docs/' . $dt->file) ?>"><i class="icon-download"></i> Full Paper</a>
                            <a class="btn btn-success" href="<?= asset('../storage/docs/' . $dt->fileAbstract) ?>"><i class="icon-download"></i> Abstract</a>

                        </div>
                        <div class="col-md-8 journal">
                            <div class="row">
                                <h3><b><?= $dt->title ?></b></h3>
                                <h6><?= $dt->author ?></h6>
                                <p class="abstract"><b>Abstract &mdash;</b> <?= $dt->abstract ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('layouts.footer')
    <!-- /footer -->

    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript">
        function checkPasswordMatch() {
            var password = $("#txtNewPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();

            if (password != confirmPassword) {
                $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
                $("#submit").prop('disabled', true);

            } else {
                $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
                $("#submit").removeAttr("disabled");
            }
        }

        $(document).ready(function() {
            $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
        });
    </script>

</body>

</html>