@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
	@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Manajemen Pengguna</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Pendukung</span> &mdash; Manajemen Pengguna</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<?php
				if (Session::has('success')) {
					echo '<div class="alert alert-success">' . Session::get("success") . '</div>';
				}
				?>
				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Manajemen Pengguna</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div style="padding-left:20px;" class="col-md-2">
						<a data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Pengguna</a>
					</div>
					<div class="col-md-2">
						<a data-toggle="modal" data-target="#myModal2" class="btn btn-primary">Atur Email Admin Paper</a>
					</div>
					<table id="tabelqu" class="table table-striped datatable-responsive">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nama</th>
								<th>Email</th>
								<!-- <th>Level</th> -->
								<th>Aksi</th>
							</tr>
						</thead>
						<?php
						foreach ($user as $dt) {
						?>
							<tr>
								<td><?= $dt->id ?></td>
								<td><?= $dt->name ?></td>
								<td><?= $dt->email ?></td>
								<!-- <td><?= $dt->hak_akses ?></td> -->
								<td>
									<a class="btn btn-primary" href="{{ url('user/sunting/') }}/<?= $dt->id ?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp;
									<a class="btn btn-danger" onclick="return confirm('User yang akan dihapus tidak dapat dikembalikan lagi. Klik OK untuk melanjutkan.');" href="{{ url('user/hapus/') }}/<?= $dt->id ?>"><i class="icon-trash"></i></a>
								</td>
							</tr>
						<?php
						}
						?>
						<tbody>
						</tbody>
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
	<script type="text/javascript">
		function checkPasswordMatch() {
			var password = $("#txtNewPassword").val();
			var confirmPassword = $("#txtConfirmPassword").val();

			if (password != confirmPassword) {
				$("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
				$("#submit").prop('disabled', true);

			} else {
				$("#divCheckPasswordMatch").html("<font color='green'>Kata sandi sudah cocok.</font>");
				$("#submit").removeAttr("disabled");
			}
		}

		$(document).ready(function() {
			$("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
		});
	</script>

</body>

</html>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Pengguna</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{url('registrasi')}}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="name" class="col-md-4 control-label">Nama</label>

						<div class="col-md-6">
							<input required id="name" autocomplete="off" type="text" class="form-control" name="name" value="">

						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Alamat Surel</label>

						<div class="col-md-6">
							<input required id="email" type="email" class="form-control" name="email" value="">

						</div>
					</div>

					<div class="form-group">
						<label for="password" class="col-md-4 control-label">Kata Sandi</label>

						<div class="col-md-6">
							<input required id="txtNewPassword" type="password" class="form-control" name="password">

						</div>
					</div>

					<div class="form-group">
						<label for="password-confirm" class="col-md-4 control-label">Konfirmasi Kata Sandi</label>

						<div class="col-md-6">
							<input required id="txtConfirmPassword" type="password" class="form-control" name="password_confirmation">

						</div>
					</div>
					<div class="form-group">
						<label for="password-confirm" class="col-md-4 control-label"></label>
						<div class="col-md-6">
							<div class="registrationFormAlert" id="divCheckPasswordMatch">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="password-confirm" class="col-md-4 control-label">Level Akses</label>

						<div class="col-md-6">
							<?php
							foreach ($level as $dt1) {
							?>
								<label class="radio-inline"><input required type="radio" name="level" value="<?= $dt1->id ?>"><?= $dt1->hak_akses ?></label>

							<?php
							}
							?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button disabled id="submit" type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-user"></i> Tambah
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<?php
foreach ($paper_admin as $pda) {
}
?>

<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Admin Paper</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{url('conf/ubah_admin_paper')}}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="name" class="col-md-4 control-label">Email Admin Paper</label>

						<div class="col-md-6">
							<div class="form-group">
								<select class="form-control" name="email" id="sel1">
									<?php
									foreach ($user as $dtuser) {
										if ($dtuser->id == $pda->id) {
											$isselected = 'selected';
										} else {
											$isselected = '';
										}
									?>
										<option {{$isselected}} value="{{$dtuser->id}}">{{$dtuser->email}}</option>
									<?php
									}
									?>
								</select>
							</div>
							<!-- <input required id="email" autocomplete="off" type="text" class="form-control" name="name" value="{{$pda->id}}"> -->
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button id="submit" type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-user"></i> Ubah
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>