<div class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="index.html"><b>ADMIN</b> PANEL</a>

		<ul class="nav navbar-nav visible-xs-block">
			<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">
		<ul class="nav navbar-nav">
			<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

		</ul>

		<p class="navbar-text"><span class="label bg-success-400">Daring</span></p>

		<ul class="nav navbar-nav navbar-right">

			<li class="dropdown dropdown-user">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<img src="<?= Config::get('global.base_url');?>assets/images/placeholder.jpg" alt="">
					<span> {{Auth::user()->name}} </span>
					<i class="caret"></i>
				</a>

				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="#"><i class="icon-user-plus"></i> Profil Saya</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-cog5"></i> Pengaturan Akun</a></li>
					<li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Keluar</a></li>
					
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
			</li>
		</ul>
	</div>
</div>