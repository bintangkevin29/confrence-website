<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Correspondence Name</th>
            <th>Correspondence Email</th>
            <th>Affiliation</th>
            <th>Paper Title</th>
            <th>Author</th>
            <th>Tracks</th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach($partisipan as $partisipans)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $partisipans->name }}</td>
            <td>{{ $partisipans->email }}</td>
            <td>{{ $partisipans->affiliation }}</td>
            <td>{{ $partisipans->title }}</td>
            <td>{{ $partisipans->author }}</td>
            <td>{{ $partisipans->tracks }}</td>
        </tr>
    @endforeach
    </tbody>
</table>