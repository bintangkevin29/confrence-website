@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Manajemen Pengguna</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Pendukung</span> &mdash; Manajemen Pengguna</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->
			<?php
			$data = $data[0];
			?>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<?php
 						if(Session::has('success'))
						{
						     echo '<div class="alert alert-success">'. Session::get("success").'</div>';
						}
						?>
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Sunting Pengguna</h5> 
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
 					<div style="padding-left:20px;">
 						<a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
 					</div>
 					<div class="panel-body">
 						
 						<form class="form-horizontal" role="form" method="POST" action="{{url('user/sunting/save')}}">
			            {{ csrf_field() }}
			    		<div class="form-group">
			    			<label for="name" class="col-md-2 control-label">Nama</label>

			    			<div class="col-md-6">
			    				<input required id="name" autocomplete="off" type="text" class="form-control" name="name" value="<?=$data->name?>">
			    				<input required id="name" autocomplete="off" type="text" class="form-control" name="id" value="<?=$data->id?>" style="display: none;">
			    			</div>
			    		</div>

			    		<div class="form-group">
			    			<label for="email" class="col-md-2 control-label">Alamat Surel</label>
			    			<div class="col-md-6">
			    				<input required id="email" type="email" class="form-control" name="email" value="<?=$data->email?>">
			    			</div>
			    		</div> 
			    		<!-- <div class="form-group">
			    			<label for="password-confirm" class="col-md-2 control-label">Level Akses</label>

			    			<div class="col-md-6">
			    				<?php
			    				$select = '';
			    					foreach($level as $dt1){
			    						if($dt1->id == $data->hak_akses)
			    						{
			    							$select = 'checked';
			    						}
			    						else{
			    							$select = '';
			    						}
			    						?>
			    					<label class="radio-inline"><input required <?=$select?> type="radio" name="level" value="<?= $dt1->id?>"><?= $dt1->hak_akses?></label>

			    						<?php
			    					}
			    				?>
			    			</div>
			    		</div> -->

			    		<div class="form-group">
			    			<div class="col-md-6 col-md-offset-4">
			    				<button id="submit" type="submit" class="btn btn-primary">
			    					<i class="fa fa-btn fa-user"></i> Submit
			    				</button>
			    			</div>
			    		</div>
			    	</form>
 					</div>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
	<script type="text/javascript">

	function checkPasswordMatch() {
	    var password = $("#txtNewPassword").val();
	    var confirmPassword = $("#txtConfirmPassword").val();

	    if (password != confirmPassword){
	        $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
	    	$("#submit").prop('disabled', true);

	    }
	    else{
	        $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
	    	$("#submit"). removeAttr("disabled");
	    }
	}

	$(document).ready(function () {
	   $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
	});

	</script>

</body>
</html>