@include('layouts.header')

<body class="navbar-bottom">
	@include('layouts.navbar')

	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('beranda') }}">
						<i class="icon-home2 position-left"></i> Beranda
					</a>
				</li>
				<li class="active">Committee</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>Pengaturan<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>

		</div>

		<!-- 1 -->

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Anniversary</span> &mdash; Slider Text</h4>
			</div>
		</div>
	</div>
	<!-- Page header /ans-->

	<!-- Page Container /ans -->
	<div class="page-container">
		<div class="page-content">
			<!-- Main sidebar -->
			@include('layouts.sidebar')

			<div class="content-wrapper">
				<!-- CONTENT WRAPPER KANAN ANS-->

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Anniversary &mdash; Text Slider</h5> 
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
 					<!-- <div style="padding-left:20px;">
 						<a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
 					</div> -->

 					<div class="panel-body">
 						<?php
 						if(Session::has('success'))
 						{
 							echo '<div class="alert alert-success">'. Session::get("success").'</div>';
 						}
 						?>
 						<form class="form-horizontal" role="form" method="POST" action="{{url('anniv/slider/update')}}">
 							{{ csrf_field() }}
 							<?php
 							foreach($textslider as $dt){
 								$about = $dt->cover_title;
 								$location = $dt->fokus_utama;
 								$countdown = $dt->cover_subtitle;
 							} 
 							?>
 							<div class="form-group">
 								<label for="name" class="col-md-2 control-label">Title 1</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="title1" value="<?= $about ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Title 2</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="title2" value="<?= $location ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Title 3</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="title3" value="<?= $countdown ?>">		
 								</div>
 							</div>  
 							<!-- <div class="form-group">
 								<label class="col-form-label col-lg-2">Foto</label>
 								<div class="col-lg-10">
 									<input type="file" class="form-control-plaintext" accept="image/x-png,image/gif,image/jpeg">
 								</div>
 							</div> -->

 							<div class="form-group">
 								<div class="col-md-6">
 									<button id="submit" type="submit" class="btn btn-primary btn-lg">
 										<i class="fa fa-btn fa-user"></i> Submit
 									</button>
 								</div>
 							</div>
 						</form>
 					</div>
 				</div>

			</div>
		</div>
	</div>
	<!-- Page Container ends -->

	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
	<script type="text/javascript">
		function checkPasswordMatch() {
			var password = $("#txtNewPassword").val();
			var confirmPassword = $("#txtConfirmPassword").val();

			if (password != confirmPassword) {
				$("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
				$("#submit").prop('disabled', true);

			} else {
				$("#divCheckPasswordMatch").html("<font color='green'>Kata sandi sudah cocok.</font>");
				$("#submit").removeAttr("disabled");
			}
		}

		$(document).ready(function() {
			$("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
		});
	</script>

</body>
 