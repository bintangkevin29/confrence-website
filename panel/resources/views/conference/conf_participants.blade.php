@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
	@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Registered Participants</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Conference</span> &mdash; Registered Participants</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Registered Participants</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2" style="margin-left:10px;">
						<div>
							<!-- <a data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Registered Participants</a> -->
							<a href="{{url('/conf/participants/export')}}" class="btn btn-primary">Export data Partisipan</a>
						</div>
					</div>
					<div class="col-md-2">
						<div>
							<!-- <a data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Registered Participants</a> -->
							<a href="{{url('../conference/download_papers')}}" class="btn btn-primary">Download semua file paper</a>
						</div>
					</div>
					<table id="tabelqu" class="table table-striped datatable-responsive">
						<thead>
							<tr>
								<th>ID</th>
								<th>Track</th>
								<th>Title</th>
								<th>Author</th>
								<th>Email</th>
								<th>Proceeding?</th>
								<th>Action</th>
							</tr>
						</thead>
						<?php
						foreach ($data as $dt) {
						?>
							<tr>
								<td><?= $dt->id ?></td>
								<td><?= $dt->title_tracks ?></td>
								<td><?= $dt->title ?></td>
								<td><?= $dt->author ?></td>
								<td><?= $dt->email ?></td>
								<td><?php echo ucwords($dt->published ?? '') ?></td>
								<td>
									<!-- <a class="btn btn-primary" href="{{ url('conf/pricing/sunting/') }}/<?= $dt->id ?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp; -->
									<a class="btn btn-success" href="<?= asset('../storage/docs/' . $dt->file) ?>"><i class="icon-download"></i> Full Paper</a>
									<a class="btn btn-success" href="<?= asset('../storage/docs/' . $dt->fileAbstract ?? '') ?>"><i class="icon-download"></i> Abstract</a>
									<a class="btn btn-info" href="<?= url('conf/participants/' . $dt->id); ?>"><i class="icon-question6"></i> Detail</a>
									<a class="btn btn-danger" onclick="return confirm('Registered Participants yang akan dihapus tidak dapat dikembalikan lagi. Klik OK untuk melanjutkan.');" href="{{ url('conf/participants/hapus/') }}/<?= $dt->id ?>"><i class="icon-trash"></i></a>
								</td>
							</tr>
						<?php
						}
						?>

						<tbody>
						</tbody>
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
	<script type="text/javascript">
		function checkPasswordMatch() {
			var password = $("#txtNewPassword").val();
			var confirmPassword = $("#txtConfirmPassword").val();

			if (password != confirmPassword) {
				$("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
				$("#submit").prop('disabled', true);

			} else {
				$("#divCheckPasswordMatch").html("<font color='green'>Kata sandi sudah cocok.</font>");
				$("#submit").removeAttr("disabled");
			}
		}

		$(document).ready(function() {
			$("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
		});
	</script>

</body>

</html>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Registered Participants</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{url('conf/pricing/add')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="name" class="col-md-4 control-label">Title</label>
						<div class="col-md-6">
							<input required id="name" autocomplete="off" type="text" class="form-control" name="title" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Desc</label>
						<div class="col-md-6">
							<input required id="shortdesc" autocomplete="off" type="text" class="form-control" name="desc" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">IDR Amount</label>
						<div class="col-md-6">
							<input required id="shortdesc" autocomplete="off" type="number" class="form-control" name="amountIDR" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">USD Amount</label>
						<div class="col-md-6">
							<input required id="shortdesc" autocomplete="off" type="number" class="form-control" name="amountUSD" value="">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button id="submit" type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-user"></i> Tambah
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>