@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
	@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Conference &mdash; Contacts</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Conference</span> &mdash; Contacts</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Conference &mdash; Contacts</h5> 
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
 					<!-- <div style="padding-left:20px;">
 						<a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
 					</div> -->

 					<div class="panel-body">
 						<?php
 						if(Session::has('success'))
 						{
 							echo '<div class="alert alert-success">'. Session::get("success").'</div>';
 						}
 						?>
 						<form class="form-horizontal" role="form" method="POST" action="{{url('conf/contacts/save')}}">
 							{{ csrf_field() }}
 							<?php
 							foreach($data as $dt){
 								$phone = $dt->phone;
 								$email = $dt->email;
 								$website = $dt->website;
 								$address = $dt->address;
 								$facebook = $dt->facebook;
 								$instagram = $dt->instagram;
 								$twitter = $dt->twitter;
 							} 
 							?>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Phone</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="phone" value="<?= $phone ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Email</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="email" class="form-control" name="email" value="<?= $email ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Website</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="website" value="<?= $website ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="email" class="col-md-2 control-label">Address</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="address" value="<?= $address ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="facebook" class="col-md-2 control-label">Facebook</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="facebook" value="<?= $facebook ?>">
 								</div>
 							</div>
 							<!-- <div class="form-group">
 								<label class="col-form-label col-lg-2">Foto</label>
 								<div class="col-lg-10">
 									<input type="file" class="form-control-plaintext" accept="image/x-png,image/gif,image/jpeg">
 								</div>
 							</div> -->

 							<div class="form-group">
 								<label for="twitter" class="col-md-2 control-label">Twitter</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="twitter" value="<?= $twitter ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<label for="instagram" class="col-md-2 control-label">
 									Instagram
 								</label>
 								<div class="col-md-6">
 									<input required autocomplete="off" type="text" class="form-control" name="instagram" value="<?= $instagram ?>">
 								</div>
 							</div>

 							<div class="form-group">
 								<div class="col-md-6">
 									<button id="submit" type="submit" class="btn btn-primary btn-lg">
 										<i class="fa fa-btn fa-user"></i> Submit
 									</button>
 								</div>
 							</div>
 						</form>
 					</div>
 				</div>
 				<!-- /basic responsive configuration -->


 				<!-- /whole row as a control -->

 			</div>
 			<!-- /main content -->

 		</div>
 		<!-- /page content -->

 	</div>
 	<!-- /page container -->


 	<!-- Footer -->
 	@include('layouts.footer')
 	<!-- /footer -->

 	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
 	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
 	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

 	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
 	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
 	<script type="text/javascript">

 		function checkPasswordMatch() {
 			var password = $("#txtNewPassword").val();
 			var confirmPassword = $("#txtConfirmPassword").val();

 			if (password != confirmPassword){
 				$("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
 				$("#submit").prop('disabled', true);

 			}
 			else{
 				$("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
 				$("#submit"). removeAttr("disabled");
 			}
 		}

 		$(document).ready(function () {
 			$("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
 		});

 	</script>

 </body>
 </html>