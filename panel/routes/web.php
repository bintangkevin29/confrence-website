<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('user', 'UserController@index');
Route::post('registrasi', 'RegistrasiController@index');
Route::get('user', 'UserController@index');
Route::get('user/hapus/{id}', 'UserController@hapus');
Route::get('user/sunting/{id}', 'UserController@sunting');
Route::post('user/sunting/save', 'UserController@update');

Route::post('conf/ubah_admin_paper', 'ConfController@ubah_admin_paper');


Route::get('anniv/about', 'AnnivController@about');
Route::post('anniv/about/save', 'AnnivController@update_about');

Route::get('anniv/slider', 'AnnivController@subslider');
Route::post('anniv/slider/update', 'AnnivController@update_subslider');
// Route::post('anniv/slider/add', 'AnnivController@add_news');
// Route::get('anniv/slider/hapus/{id}', 'AnnivController@hapus_news');
Route::get('anniv/slider/sunting/{id}', 'AnnivController@sunting_teksslider');
// Route::post('anniv/slider/sunting/save', 'AnnivController@update_news');


Route::get('anniv/news', 'AnnivController@news');
Route::post('anniv/news/add', 'AnnivController@add_news');
Route::get('anniv/news/hapus/{id}', 'AnnivController@hapus_news');
Route::get('anniv/news/sunting/{id}', 'AnnivController@sunting_news');
Route::post('anniv/news/sunting/save', 'AnnivController@update_news');


Route::get('anniv/committee', 'AnnivController@committee');
Route::post('anniv/committee/add', 'AnnivController@add_committee');
Route::get('anniv/committee/hapus/{id}', 'AnnivController@hapus_committee');
Route::get('anniv/committee/sunting/{id}', 'AnnivController@sunting_committee');
Route::post('anniv/committee/sunting/save', 'AnnivController@update_committee');


Route::get('anniv/vip', 'AnnivController@vip');
Route::post('anniv/vip/add', 'AnnivController@add_vip');
Route::get('anniv/vip/hapus/{id}', 'AnnivController@hapus_vip');
Route::get('anniv/vip/sunting/{id}', 'AnnivController@sunting_vip');
Route::post('anniv/vip/sunting/save', 'AnnivController@update_vip');

Route::get('anniv/events', 'AnnivController@events');
Route::post('anniv/events/add', 'AnnivController@add_events');
Route::get('anniv/events/hapus/{id}', 'AnnivController@hapus_events');
Route::get('anniv/events/sunting/{id}', 'AnnivController@sunting_events');
Route::post('anniv/events/sunting/save', 'AnnivController@update_events');

Route::get('anniv/contacts', 'AnnivController@contacts');
Route::post('anniv/contacts/save', 'AnnivController@update_contacts');


Route::get('conf/about', 'ConfController@about');
Route::post('conf/about/save', 'ConfController@update_about');

Route::get('conf/policy', 'ConfController@policy');
Route::post('conf/policy/save', 'ConfController@update_policy');

Route::get('conf/venue', 'ConfController@venue');
Route::post('conf/venue/save', 'ConfController@update_venue');

Route::get('conf/conferenceguide', 'ConfController@conferenceguide');
Route::post('conf/conferenceguide/save', 'ConfController@update_conferenceguide');
Route::get('conf/guide', 'ConfController@guide');
Route::post('conf/guide/save', 'ConfController@update_guide');

Route::get('conf/topics', 'ConfController@topics');
Route::post('conf/topics/save', 'ConfController@update_topics');

Route::get('conf/news', 'ConfController@news');
Route::post('conf/news/add', 'ConfController@add_news');
Route::get('conf/news/hapus/{id}', 'ConfController@hapus_news');
Route::get('conf/news/sunting/{id}', 'ConfController@sunting_news');
Route::post('conf/news/sunting/save', 'ConfController@update_news');


Route::get('conf/speakers', 'ConfController@committee');
Route::post('conf/speakers/add', 'ConfController@add_committee');
Route::get('conf/speakers/hapus/{id}', 'ConfController@hapus_committee');
Route::get('conf/speakers/sunting/{id}', 'ConfController@sunting_committee');
Route::post('conf/speakers/sunting/save', 'ConfController@update_committee');

Route::get('conf/speakers_type', 'ConfController@speakers_type');
Route::post('conf/speakers_type/add', 'ConfController@add_speakers_type');
Route::get('conf/speakers_type/hapus/{id}', 'ConfController@hapus_speakers_type');
Route::get('conf/speakers_type/sunting/{id}', 'ConfController@sunting_speakers_type');
Route::post('conf/speakers_type/sunting/save', 'ConfController@update_speakers_type');


Route::get('conf/vip', 'ConfController@vip');
Route::post('conf/vip/add', 'ConfController@add_vip');
Route::get('conf/vip/hapus/{id}', 'ConfController@hapus_vip');
Route::get('conf/vip/sunting/{id}', 'ConfController@sunting_vip');
Route::post('conf/vip/sunting/save', 'ConfController@update_vip');

Route::get('conf/events', 'ConfController@events');
Route::post('conf/events/add', 'ConfController@add_events');
Route::get('conf/events/hapus/{id}', 'ConfController@hapus_events');
Route::get('conf/events/sunting/{id}', 'ConfController@sunting_events');
Route::post('conf/events/sunting/save', 'ConfController@update_events');

Route::get('conf/contacts', 'ConfController@contacts');
Route::post('conf/contacts/save', 'ConfController@update_contacts');

Route::get('conf/download_papers', 'ConfController@download_papers');

Route::get('conf/participants/nonpresenter', 'ConfController@participants_nonpresenter');
Route::get('conf/participants', 'ConfController@participants');
Route::get('conf/participants/export', 'ConfController@laporanExcel');
// Route::post('conf/participants/add', 'ConfController@add_participants');
Route::get('conf/participants/hapus/{id}', 'ConfController@hapus_participants');
Route::get('conf/participants/{id}', 'ConfController@detail_participants');
// Route::get('conf/participants/sunting/{id}', 'ConfController@sunting_participants');
// Route::post('conf/participants/sunting/save', 'ConfController@update_participants');
Route::get('conf/booths', 'ConfController@booth');

Route::get('conf/pricing', 'ConfController@pricing');
Route::post('conf/pricing/add', 'ConfController@add_pricing');
Route::get('conf/pricing/hapus/{id}', 'ConfController@hapus_pricing');
Route::get('conf/pricing/sunting/{id}', 'ConfController@sunting_pricing');
Route::post('conf/pricing/sunting/save', 'ConfController@update_pricing');

Route::get('conf/tracks', 'ConfController@tracks');
Route::post('conf/tracks/add', 'ConfController@add_tracks');
Route::get('conf/tracks/hapus/{id}', 'ConfController@hapus_tracks');
Route::get('conf/tracks/sunting/{id}', 'ConfController@sunting_tracks');
Route::post('conf/tracks/sunting/save', 'ConfController@update_tracks');

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Route::get('/home', 'HomeController@index')->name('home');
