<?php

namespace App\Exports;

use App\Participants;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ParticipantsExport implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('exportPartisipan', [
            'partisipan' => Participants::all()
        ]);
    }
}
