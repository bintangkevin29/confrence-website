<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    protected $table = 'presenter_tracks';

    protected $fillable = ['name', 'email', 'affiliaton', 'title', 'abstract', 'author','tracks'];
}
