<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        DB::enableQueryLog();
        $users = DB::table('users')
            ->select('users.id as id', 'name', 'email', 'level_akses.hak_akses as hak_akses')
            ->join('level_akses', 'users.hak_akses', '=', 'level_akses.id')
            ->get();

        $dt_admin = DB::table('conf_paper_admin')
            ->select('*')
            ->get();
        
            foreach($dt_admin as $dta)
            {
                $admin = $dta->id_user;
            }

        $paper_admin = DB::table('users')
        ->select('*')
        ->where('id','=',$admin)
        ->get();
        // dd(DB::getQueryLog());

        $level = DB::table('level_akses')
            ->get();
        return view('user', ['user' => $users, 'level' => $level, 'paper_admin' => $paper_admin]);
        // var_dump($users);
    }

    public function hapus($id)
    {
        DB::table('users')->where('id', '=', $id)->delete();
        return redirect('user');
    }

    public function sunting($id)
    {
        $data = DB::table('users')->where('id', '=', $id)->get();
        $level = DB::table('level_akses')
            ->get();
        return view('sunting', ['data' => $data, 'level' => $level]);
        // var_dump($data);
    }

    public function update(Request $request)
    {
        $data = DB::table('users')->where('id', '=', $request->id)->get();
        $level = DB::table('level_akses')
            ->get();
        DB::table('users')
            ->where('id', '=', $request->id)
            ->update(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'hak_akses' => $request->level
                ]
            );
        Session::flash('success', 'Berhasil Menyunting Data');
        return redirect('user/sunting/' . $request->id)->with(['sukses' => TRUE]);;
        // var_dump($data);
    }
}
