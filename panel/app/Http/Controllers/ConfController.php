<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;


use Excel;
use App\Exports\ParticipantsExport;


class ConfController extends Controller
{
	function about()
	{
		$data = DB::table('itb_about')
			->select('*')
			->where('display_in', '=', 2)
			->get();
		return view('conference.conf_about', ['data' => $data]);
	}

	function update_about(Request $request)
	{
		DB::table('itb_about')
			->where('display_in', '=', 2)
			->update(
				[
					'about' => $request->about,
					'location' => $request->location,
					'countdown' => $request->countdown,
					'title' => $request->title,
					'short_title' => $request->short_title,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/about/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function news()
	{
		$news = DB::table('itb_news')
			->join(
				'users',
				function ($join) {
					$join->on('itb_news.id_author', '=', 'users.id');
				}
			)
			->select('*')
			->where('display_in', '=', 2)
			->get();
		return view('conference.conf_news', ['news' => $news]);
	}

	function add_news(Request $request)
	{
		$user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/blog/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		$namafile_large = $tujuan_upload . '/large_' . $nama;
		Image::make(Input::file('file'))->fit(754, 462)->save($namafile);
		Image::make(Input::file('file'))->fit(1161, 590)->save($namafile_large);
		Image::make(Input::file('file'))->fit(100, 61)->save($namafile_thumb);
		DB::table('itb_news')->insert(
			[
				'news_title' => $request->judul,
				'news_contain' => $request->artikel,
				'id_author' => $user->id,
				'news_time' => date('Y-m-d H:i:s', time()),
				'news_photo' => $nama,
				'display_in' => 2,
			]
		);

		return redirect('conf/news/');
	}

	public function hapus_news($id)
	{
		DB::table('itb_news')->where('id_news', '=', $id)->delete();
		return redirect('conf/news/');
	}

	public function sunting_news($id)
	{
		$data = DB::table('itb_news')->where('id_news', '=', $id)->get();
		$level = DB::table('level_akses')
			->get();
		return view('conference.conf_news_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_news(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_news')
				->where('id_news', '=', $request->id)
				->update(
					[
						'news_title' => $request->judul,
						'news_contain' => $request->artikel,
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/blog/';
			$nama = time() . '_' . $file->cubrid_get_client_info()->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(754, 462)->save($namafile);
			Image::make(Input::file('file'))->fit(100, 61)->save($namafile_thumb);
			DB::table('itb_news')
				->where('id_news', '=', $request->id)
				->update(
					[
						'news_title' => $request->judul,
						'news_contain' => $request->artikel,
						'news_photo' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/news/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function committee()
	{
		$committee = DB::table('itb_speakers')
			->join(
				'conf_speakers_type',
				function ($join) {
					$join->on('itb_speakers.id_type', '=', 'conf_speakers_type.id');
				}
			)
			->select('*')

			->get();
		$type = DB::table('conf_speakers_type')
			->select('*')
			->get();
		return view('conference.conf_committee', ['committee' => $committee, 'type' => $type]);
	}


	function add_committee(Request $request)
	{
		$user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/speakers/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		Image::make(Input::file('file'))->fit(382, 385)->save($namafile);
		if ($request->confirmed == '1') {
			$confirmed = 1;
		} else {
			$confirmed = 2;
		}
		DB::table('itb_speakers')->insert(
			[
				'name_of_speakers' => $request->nama,
				'short_desc' => $request->shortdesc,
				'speakers_longdesc' => $request->longdesc,
				'speakers_img' => $nama,
				'id_type' => $request->type,
				'confirmed' => $confirmed,
			]
		);

		return redirect('conf/speakers/');
	}

	public function hapus_committee($id)
	{
		DB::table('itb_speakers')->where('id_speakers', '=', $id)->delete();
		return redirect('conf/speakers/');
	}

	public function sunting_committee($id)
	{
		$data_type = DB::table('conf_speakers_type')
			->select('*')
			->get();
		$data = DB::table('itb_speakers')
			->join(
				'conf_speakers_type',
				function ($join) {
					$join->on('itb_speakers.id_type', '=', 'conf_speakers_type.id');
				}
			)->where('id_speakers', '=', $id)->get();
		return view('conference.conf_committee_sunting', compact('data', 'data_type'));
		// var_dump($data);
	}

	public function update_committee(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_speakers')
				->where('id_speakers', '=', $request->id)
				->update(
					[
						'name_of_speakers' => $request->nama,
						'short_desc' => $request->shortdesc,
						'speakers_longdesc' => $request->longdesc,
						'id_type' => $request->type,
						'confirmed' => $request->confirmed
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/speakers/';
			$nama = time() . '_' . $file->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(352, 385)->save($namafile);
			DB::table('itb_speakers')
				->where('id_speakers', '=', $request->id)
				->update(
					[
						'name_of_speakers' => $request->nama,
						'short_desc' => $request->shortdesc,
						'speakers_longdesc' => $request->longdesc,
						'speakers_img' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/speakers/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function vip()
	{
		$data = DB::table('itb_vip')
			->select('*')
			->get();
		return view('conference.conf_vip', ['data' => $data]);
	}

	function add_vip(Request $request)
	{
		// $user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/brands/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		Image::make(Input::file('file'))->fit(310, 310)->save($namafile);
		DB::table('itb_vip')->insert(
			[
				'name_vip' => $request->nama,
				'desc_vip' => $request->desc,
				'img_vip' => $nama,
			]
		);

		return redirect('conf/vip/');
	}

	public function hapus_vip($id)
	{
		DB::table('itb_vip')->where('id_vip', '=', $id)->delete();
		return redirect('conf/vip/');
	}

	public function sunting_vip($id)
	{
		$data = DB::table('itb_vip')->where('id_vip', '=', $id)->get();
		return view('conference.conf_vip_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_vip(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_vip')
				->where('id_vip', '=', $request->id)
				->update(
					[
						'name_vip' => $request->nama,
						'desc_vip' => $request->desc,
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/brands/';
			$nama = time() . '_' . $file->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(310, 310)->save($namafile);
			DB::table('itb_vip')
				->where('id_vip', '=', $request->id)
				->update(
					[
						'name_vip' => $request->nama,
						'desc_vip' => $request->desc,
						'img_vip' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/vip/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function events()
	{
		$data = DB::table('event_dates')
			->select('*')
			->where('display_in', '=', 3)
			->get();
		return view('conference.conf_events', ['data' => $data]);
	}

	function add_events(Request $request)
	{
		// $user = Auth::user();
		DB::table('event_dates')->insert(
			[
				'waktu_event' => $request->date,
				'display_in' => 3,
				'title' => $request->event,
				'event_desc' => $request->desc,
			]
		);

		return redirect('conf/events/');
	}

	public function hapus_events($id)
	{
		DB::table('event_dates')->where('id_events', '=', $id)->delete();
		return redirect('conf/events/');
	}

	public function sunting_events($id)
	{
		$data = DB::table('event_dates')->where('id_events', '=', $id)->get();
		return view('conference.conf_events_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_events(Request $request)
	{
		DB::table('event_dates')
			->where('id_events', '=', $request->id)
			->update(
				[
					'waktu_event' => $request->date,
					'display_in' => 1,
					'title' => $request->event,
					'event_desc' => $request->desc
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/events/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function contacts()
	{
		$data = DB::table('itb_contact')
			->select('*')
			// ->where('display_in', '=', 2)
			->get();
		return view('conference.conf_contact', ['data' => $data]);
	}

	function update_contacts(Request $request)
	{
		DB::table('itb_contact')
			// ->where('display_in', '=', 2)
			->update(
				[
					'phone' => $request->phone,
					'address' => $request->address,
					'website' => $request->website,
					'email' => $request->email,
					'facebook' => $request->facebook,
					'twitter' => $request->twitter,
					'instagram' => $request->instagram
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/contacts/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function participants()
	{
		$data = DB::table('conf_participants')
			->join('conf_tracks', function ($join) {
				$join->on('conf_participants.id_tracks', '=', 'conf_tracks.id_tracks');
			})
			->select('*')
			->get();


		return view('conference.conf_participants', ['data' => $data]);
	}

	function participants_nonpresenter()
	{
		$data = DB::table('conf_participants_non_presenter')
			->leftJoin('conf_tracks', function ($join) {
				$join->on('conf_participants_non_presenter.id_tracks', '=', 'conf_tracks.id_tracks');
			})
			->select('*')
			->get();

		$title = 'Participants';


		return view('conference.conf_participants_non_presenter', compact('data'));
	}

	function booth()
	{
		$data = DB::table('expo_booth')
			->select('*')
			->get();

		$title = 'Booth';


		return view('conference.expo_booth', compact('data', 'title'));
	}

	// function add_participants(Request $request){
	// 	// $user = Auth::user();
	// 	DB::table('conf_participants')->insert(
	// 		[
	// 			'price_shortdesc' => $request->desc,
	// 			'idr_amount' => $request->amountIDR,
	// 			'usd_amount' => $request->amountUSD,
	// 			'price_title' => $request->title,
	// 		]);

	// 	return redirect('conf/participants/');
	// }

	public function hapus_participants($id)
	{
		$data = DB::table('conf_participants')
			->where('id', '=', $id)
			->select('*')
			->get();
		foreach ($data as $dt) {
			$file = $dt->file;
		}
		unlink('../storage/docs/' . $file);
		DB::table('conf_participants')->where('id', '=', $id)->delete();
		return redirect('conf/participants/');
	}

	public function detail_participants($id)
	{
		$data = DB::table('conf_participants')
			->where('id', '=', $id)
			->select('*')
			->get();
		return view('detail_participants', compact('data'));
	}

	// public function sunting_participants($id){
	// 	$data = DB::table('conf_participants')->where('id_participants', '=', $id)->get();
	// 	return view('conference.conf_participants_sunting', ['data' => $data]);
	//     // var_dump($data);
	// }

	// public function update_participants(Request $request){
	// 	DB::table('conf_participants')
	// 	->where('id_participants', '=', $request->id)
	// 	->update(
	// 		[
	// 			'price_shortdesc' => $request->desc,
	// 			'idr_amount' => $request->amountIDR,
	// 			'usd_amount' => $request->amountUSD,
	// 			'price_title' => $request->title,

	// 		]);


	// 	Session::flash('success', 'Berhasil Menyunting Data');
	// 	return redirect('conf/participants/sunting/'.$request->id)->with( ['sukses' => TRUE] );;
	//     // var_dump($data);

	// }
	function pricing()
	{
		$data = DB::table('conf_pricing')
			->select('*')
			->get();
		return view('conference.conf_pricing', ['data' => $data]);
	}

	function add_pricing(Request $request)
	{
		// $user = Auth::user();
		DB::table('conf_pricing')->insert(
			[
				'price_shortdesc' => $request->desc,
				'idr_amount' => $request->amountIDR,
				'usd_amount' => $request->amountUSD,
				'price_title' => $request->title,
			]
		);

		return redirect('conf/pricing/');
	}

	public function hapus_pricing($id)
	{
		DB::table('conf_pricing')->where('id_pricing', '=', $id)->delete();
		return redirect('conf/pricing/');
	}

	public function sunting_pricing($id)
	{
		$data = DB::table('conf_pricing')->where('id_pricing', '=', $id)->get();
		return view('conference.conf_pricing_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_pricing(Request $request)
	{
		DB::table('conf_pricing')
			->where('id_pricing', '=', $request->id)
			->update(
				[
					'price_shortdesc' => $request->desc,
					'idr_amount' => $request->amountIDR,
					'usd_amount' => $request->amountUSD,
					'price_title' => $request->title,

				]
			);


		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/pricing/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);

	}

	function tracks()
	{
		$data = DB::table('conf_tracks')
			->select('*')
			->get();
		return view('conference.conf_tracks', ['data' => $data]);
	}

	function add_tracks(Request $request)
	{
		// $user = Auth::user();
		DB::table('conf_tracks')->insert(
			[
				'title_tracks' => $request->tracks,
				'code' => $request->code,
			]
		);

		return redirect('conf/tracks/');
	}

	public function hapus_tracks($id)
	{
		DB::table('conf_tracks')->where('id_tracks', '=', $id)->delete();
		return redirect('conf/tracks/');
	}

	public function sunting_tracks($id)
	{
		$data = DB::table('conf_tracks')->where('id_tracks', '=', $id)->get();
		return view('conference.conf_tracks_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_tracks(Request $request)
	{
		DB::table('conf_tracks')
			->where('id_tracks', '=', $request->id)
			->update(
				[
					'title_tracks' => $request->tracks,
					'code' => $request->code,
					'id_tracks' => $request->id,

				]
			);


		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/tracks/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);

	}
	function policy()
	{
		$data = DB::table('conf_policy')
			->select('*')
			->get();
		return view('conference.conf_policy', ['data' => $data]);
	}

	function venue() //ans
	{
		$data = DB::table('conf_venue')
			->select('*')
			->where('disp_venue', '=', 1)
			->get();
		return view('conference.conf_venue', ['data' => $data]);
	}

	function update_policy(Request $request)
	{
		DB::table('conf_policy')
			->where('id', '=', 1)
			->update(
				[
					'desc' => $request->about,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/policy/')->with(['sukses' => TRUE]);;
		// var_dump($data);	
	}

	function update_venue(Request $request)
	{
		DB::table('conf_venue')
			->where('disp_venue', '=', 1)
			->update(
				[
					'desc_venue' => $request->about,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/venue/')->with(['sukses' => TRUE]);
		// var_dump($data);	
	}
	function guide()
	{
		$data = DB::table('conf_guide')
			->select('*')
			->get();
		return view('conference.conf_guide', ['data' => $data]);
	}

	function conferenceguide()
	{
		$data = DB::table('conf_conference_guide')
			->select('*')
			->get();
		return view('conference.conf_conference_guide', ['data' => $data]);
	}

	function update_guide(Request $request)
	{
		DB::table('conf_guide')
			->where('id', '=', 1)
			->update(
				[
					'desc' => $request->about,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/guide/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function update_conferenceguide(Request $request)
	{
		DB::table('conf_conference_guide')
			->where('id', '=', 1)
			->update(
				[
					'desc' => $request->about,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/conferenceguide/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function topics()
	{
		$data = DB::table('conf_topics')
			->select('*')
			->get();
		return view('conference.conf_topics', ['data' => $data]);
	}

	function update_topics(Request $request)
	{
		DB::table('conf_topics')
			->where('id', '=', 1)
			->update(
				[
					'desc' => $request->about,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/topics/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function speakers_type()
	{
		$speakers_type = DB::table('conf_speakers_type')
			->select('*')
			->get();
		return view('conference.conf_speakers_type', ['speakers_type' => $speakers_type]);
	}

	function add_speakers_type(Request $request)
	{
		DB::table('conf_speakers_type')->insert(
			[
				'desc' => $request->nama,
			]
		);

		return redirect('conf/speakers_type/');
	}

	public function hapus_speakers_type($id)
	{
		DB::table('conf_speakers_type')->where('id', '=', $id)->delete();
		return redirect('conf/speakers_type/');
	}

	public function sunting_speakers_type($id)
	{
		$data = DB::table('conf_speakers_type')->where('id', '=', $id)->get();
		return view('conference.conf_speakers_type_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_speakers_type(Request $request)
	{
		DB::table('conf_speakers_type')
			->where('id', '=', $request->id)
			->update(
				[
					'desc' => $request->nama,
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('conf/speakers_type/sunting/' . $request->id)->with(['sukses' => TRUE]);
		// var_dump($data);
	}

	public function laporanExcel()
	{
		return (new ParticipantsExport)->download('Participants_' . time() . '.xlsx');
	}

	public function ubah_admin_paper(Request $request)
	{
		DB::table('conf_paper_admin')
			->update(
				[
					'id_user' => $request->email,
				]
			);
		Session::flash('success', 'Berhasil Menyunting Data Email Admin Paper');

		return redirect('user');
	}
}
