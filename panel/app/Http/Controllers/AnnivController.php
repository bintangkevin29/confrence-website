<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;

class AnnivController extends Controller
{
	function about()
	{
		$data = DB::table('itb_about')
			->select('*')
			->where('display_in', '=', 1)
			->get();
		return view('anniv_about', ['data' => $data]);
	}

	function update_about(Request $request)
	{
		DB::table('itb_about')
			->where('display_in', '=', 1)
			->update(
				[
					'about' => $request->about,
					'location' => $request->location,
					'countdown' => $request->countdown,
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/about/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}


	function subslider()
	{
		$textslider = DB::TABLE('covertitle')
			->select('*')
			->get();

		return view('anniv_subslider', ['textslider' => $textslider]);
	}

	public function update_subslider(Request $request)
	{     
		DB::table('covertitle')->update(
			[
				'cover_title' => $request->title1,
				'fokus_utama' => $request->title2,
				'cover_subtitle' => $request->title3,
			]
		);
		Session::flash('success', 'Berhasil Menyunting Data');

		return redirect('anniv/slider/');
	}

	function add_slider(Request $request)
	{
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/bg/';
		$nama = 'slider_' . time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		$namafile_large = $tujuan_upload . '/large_' . $nama;
		Image::make(Input::file('file'))->save($namafile_large);
		DB::table('covertitle')->insert(
			[
				'cover_title' => $request->title,
				'fokus_utama' => $request->highlight,
				'cover_subtitle' => $request->highlight,
			]
		);
		Session::flash('success', 'Berhasil Menyunting Data');

		return redirect('anniv/slider/');
	}

	function news()
	{
		$news = DB::table('itb_news')
			->join(
				'users',
				function ($join) {
					$join->on('itb_news.id_author', '=', 'users.id');
				}
			)
			->select('*')
			->where('display_in', '=', 1)
			->get();
		return view('anniv_news', ['news' => $news]);
	}

	function add_news(Request $request)
	{
		$user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/blog/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		$namafile_large = $tujuan_upload . '/large_' . $nama;
		Image::make(Input::file('file'))->fit(754, 462)->save($namafile);
		Image::make(Input::file('file'))->fit(1161, 590)->save($namafile_large);
		Image::make(Input::file('file'))->fit(100, 61)->save($namafile_thumb);
		DB::table('itb_news')->insert(
			[
				'news_title' => $request->judul,
				'news_contain' => $request->artikel,
				'id_author' => $user->id,
				'news_time' => date('Y-m-d H:i:s', time()),
				'news_photo' => $nama,
				'display_in' => 1,
			]
		);

		return redirect('anniv/news/');
	}

	public function hapus_news($id)
	{
		DB::table('itb_news')->where('id_news', '=', $id)->delete();
		return redirect('anniv/news/');
	}

	public function sunting_news($id)
	{
		$data = DB::table('itb_news')->where('id_news', '=', $id)->get();
		$level = DB::table('level_akses')
			->get();
		return view('anniv_news_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_news(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_news')
				->where('id_news', '=', $request->id)
				->update(
					[
						'news_title' => $request->judul,
						'news_contain' => $request->artikel,
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/blog/';
			$nama = time() . '_' . $file->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(754, 462)->save($namafile);
			Image::make(Input::file('file'))->fit(100, 61)->save($namafile_thumb);
			DB::table('itb_news')
				->where('id_news', '=', $request->id)
				->update(
					[
						'news_title' => $request->judul,
						'news_contain' => $request->artikel,
						'news_photo' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/news/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function committee()
	{
		$committee = DB::table('itb_committee')
			->select('*')
			->get();
		return view('anniv_committee', ['committee' => $committee]);
	}

	function add_committee(Request $request)
	{
		$user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/speakers/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		Image::make(Input::file('file'))->fit(382, 385)->save($namafile);
		DB::table('itb_committee')->insert(
			[
				'name_of_speakers' => $request->nama,
				'short_desc' => $request->shortdesc,
				'speakers_longdesc' => $request->longdesc,
				'speakers_img' => $nama,
			]
		);

		return redirect('anniv/committee/');
	}

	public function hapus_committee($id)
	{
		DB::table('itb_committee')->where('id_speakers', '=', $id)->delete();
		return redirect('anniv/committee/');
	}

	public function sunting_committee($id)
	{
		$data = DB::table('itb_committee')->where('id_speakers', '=', $id)->get();
		return view('anniv_committee_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_committee(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_committee')
				->where('id_speakers', '=', $request->id)
				->update(
					[
						'name_of_speakers' => $request->nama,
						'short_desc' => $request->shortdesc,
						'speakers_longdesc' => $request->longdesc,
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/speakers/';
			$nama = time() . '_' . $file->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(352, 385)->save($namafile);
			DB::table('itb_committee')
				->where('id_speakers', '=', $request->id)
				->update(
					[
						'name_of_speakers' => $request->nama,
						'short_desc' => $request->shortdesc,
						'speakers_longdesc' => $request->longdesc,
						'speakers_img' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/committee/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function vip()
	{
		$data = DB::table('itb_vip')
			->select('*')
			->get();
		return view('anniv_vip', ['data' => $data]);
	}

	function add_vip(Request $request)
	{
		// $user = Auth::user();
		$file = $request->file('file');
		$tujuan_upload = '../assets/img/brands/';
		$nama = time() . '_' . $file->getClientOriginalName();
		$namafile = $tujuan_upload . '/' . $nama;
		$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
		Image::make(Input::file('file'))->fit(310, 310)->save($namafile);
		DB::table('itb_vip')->insert(
			[
				'name_vip' => $request->nama,
				'desc_vip' => $request->desc,
				'img_vip' => $nama,
			]
		);

		return redirect('anniv/vip/');
	}

	public function hapus_vip($id)
	{
		DB::table('itb_vip')->where('id_vip', '=', $id)->delete();
		return redirect('anniv/vip/');
	}

	public function sunting_vip($id)
	{
		$data = DB::table('itb_vip')->where('id_vip', '=', $id)->get();
		return view('anniv_vip_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_vip(Request $request)
	{
		if ($request->file == '') {
			DB::table('itb_vip')
				->where('id_vip', '=', $request->id)
				->update(
					[
						'name_vip' => $request->nama,
						'desc_vip' => $request->desc,
					]
				);
		} else {
			$file = $request->file('file');
			$tujuan_upload = '../assets/img/brands/';
			$nama = time() . '_' . $file->getClientOriginalName();
			$namafile = $tujuan_upload . '/' . $nama;
			$namafile_thumb = $tujuan_upload . '/thumb_' . $nama;
			Image::make(Input::file('file'))->fit(310, 310)->save($namafile);
			DB::table('itb_vip')
				->where('id_vip', '=', $request->id)
				->update(
					[
						'name_vip' => $request->nama,
						'desc_vip' => $request->desc,
						'img_vip' => $nama,

					]
				);
		}

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/vip/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function events()
	{
		$data = DB::table('event_dates')
			->select('*')
			->where('display_in', '=', 1)
			->get();
		return view('anniv_events', ['data' => $data]);
	}

	function add_events(Request $request)
	{
		// $user = Auth::user();
		DB::table('event_dates')->insert(
			[
				'waktu_event' => $request->date,
				'display_in' => 1,
				'title' => $request->event,
				'event_desc' => $request->desc,
			]
		);

		return redirect('anniv/events/');
	}

	public function hapus_events($id)
	{
		DB::table('event_dates')->where('id_events', '=', $id)->delete();
		return redirect('anniv/events/');
	}

	public function sunting_events($id)
	{
		$data = DB::table('event_dates')->where('id_events', '=', $id)->get();
		return view('anniv_events_sunting', ['data' => $data]);
		// var_dump($data);
	}

	public function update_events(Request $request)
	{
		DB::table('event_dates')
			->where('id_events', '=', $request->id)
			->update(
				[
					'waktu_event' => $request->date,
					'display_in' => 1,
					'title' => $request->event,
					'event_desc' => $request->desc
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/events/sunting/' . $request->id)->with(['sukses' => TRUE]);;
		// var_dump($data);
	}

	function contacts()
	{
		$data = DB::table('itb_contact')
			->select('*')
			->where('display_in', '=', 1)
			->get();
		return view('anniv_contact', ['data' => $data]);
	}

	function update_contacts(Request $request)
	{
		DB::table('itb_contact')
			->where('display_in', '=', 1)
			->update(
				[
					'phone' => $request->phone,
					'address' => $request->address,
					'website' => $request->website,
					'email' => $request->email,
				]
			);

		Session::flash('success', 'Berhasil Menyunting Data');
		return redirect('anniv/contacts/')->with(['sukses' => TRUE]);;
		// var_dump($data);
	}
}
