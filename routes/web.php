<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/contacts', 'ContactsController@index');
Route::get('/events', 'EventsController@index');
Route::get('/news', 'NewsController@index');
Route::get('/news/{id}', 'NewsController@detail');
Route::get('/conference/home', 'ConferenceController@index');
Route::get('/expo', 'ConferenceController@expo');
Route::get('/expo/register', 'ConferenceController@expoRegister');
Route::get('/committee', 'CommitteeController@index');
Route::get('/conference/speakers', 'ConferenceController@speakers');
Route::get('/conference/papers', 'ConferenceController@papers');
Route::get('/conference/submission', 'ConferenceController@submission');
Route::get('/conference/conferenceguideline', 'ConferenceController@conferenceguideline');
Route::get('/conference/cnews', 'ConferenceController@cnews');
Route::get('/conference/contacts', 'ConferenceController@contacts');
Route::post('/conference/contacts_send', 'ConferenceController@contacts_send');

Route::resource('/conference/cpapersub', 'CPaperSubController');
Route::get('/conference/cparticipants', 'CPaperSubController@participants');
Route::post('/conference/cpapersub/add', 'CPaperSubController@add');
Route::post('/conference/cpapersub/addParticipants', 'CPaperSubController@addParticipants');
Route::get('/conference/register', 'IndexController@register');
Route::get('/conference/download_papers', 'ConferenceController@download_papers');

Route::post('/expo/add', 'CPaperSubController@addExpo');

Route::get('/tes_kirim', 'CPaperSubController@tes_kirim');

//email
Route::get('/email', function () {
    return view('send_email');
});
Route::post('/sendEmail', 'Email@sendEmail');