@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')
<!--page title section-->
<section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="inner_cover_content">
                    <h3>
                        News <small> - International Conference</small>
                    </h3>
                </div>
            </div>
        </div>

        <div class="breadcrumbs">
            <ul>
                <li><a href="{{ url('/')}}">Home</a>  
                    <li><a href="{{ url('conference/speakers')}}"><small>Speakers</small></a></li>
                    <li><a href="{{ url('conference/papers')}}"><small>Call for Papers</small></a></li>
                    <li><a href="{{ url('conference/submission')}}"><small>Submission Guideline</small></a></li>
                    <li><a href="{{ url('conference/cnews')}}"><small><b>News</b></small></a></li>
                    <li><a href="{{ url('conference/register')}}"><small>Registration</small></a></li>
                    <li><a href="{{ url('conference/contacts')}}"><small>Contacts</small></a></li>
                </ul>

            </ul>
        </div>
    </div>
</section>
<!--page title section end-->


<!--events section -->
<section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <!--blog section start -->
                <div class="col-12 col-md-8">
                    <?php
                    foreach ($news as $dnews) {
                        ?>
                        <a href="{{url('news/'.$dnews->id_news)}}">
                            <div class="blog_card">
                                <img src="{{asset('assets/img/blog/')}}{{'/'.$dnews->news_photo}}" alt="blog News ">
                                <div class="blog_box_data">
                                    <span class="blog_date">
                                        <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?>
                                    </span>
                                    <h5>
                                        <?=ucwords($dnews->news_title)?><div class="blog_meta">
                                            <small>By <?=$dnews->name?></small>
                                        </div>
                                    </h5>
                                    <p class="blog_word">
                                        <?=substr($dnews->news_contain, 0, 140)?>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                    echo '<center>'.$news->links().'</center>';

                    ?>


                    <!--pagenition menu-->
                    <!-- <div class="mt70 mb70">
                        <ul class="nav_menu">
                            <li class="active"><span>01</span></li>
                            <li><a href="#">02</a></li>
                            <li><a href="#">03</a></li>
                        </ul>
                    </div> -->
                    <!--pageintion -->

                </div>
                <!--blog section end-->

                <!--sidebar section -->
                <div class="col-12 col-md-4">
                    <div class="sidebar">
                        <!-- <div class="widget widget_search">
                            <div class="search-form">
                                <input type="text" class="search-field" placeholder="Search">
                            </div>
                        </div> -->
                        <!-- <div class="widget widget_categories">
                            <h4 class="widget-title">
                                Archives
                            </h4>
                            <ul>
                                <li><a href="#">March 2017</a></li>
                                <li><a href="#">April 2017</a></li>
                                <li><a href="#">May 2017</a></li>
                                <li><a href="#">June 2017</a></li>
                            </ul>
                        </div>
                        <div class="widget widget_categories">
                            <h4 class="widget-title">
                                Categories
                            </h4>
                            <ul>
                                <li><a href="#">Uncategorized</a></li>
                                <li><a href="#">Usefull Information</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Marketing</a></li>
                                <li><a href="#">Conference & Press</a></li>
                            </ul>
                        </div>
                    -->
                    <div class="widget widget_latest_post">
                        <h4 class="widget-title">
                            Latest Posts
                        </h4>
                        <ul>
                            <?php
                            foreach ($news as $dnews) {
                                ?>
                                <a href="{{url('news/'.$dnews->id_news)}}">
                                    <li>
                                        <div class="widget_recent_posts">
                                            <img src="{{asset('assets/img/blog/')}}{{'/thumb_'.$dnews->news_photo}}" alt="news">
                                            <div class="content">
                                                <h6><?=ucwords($dnews->news_title)?></h6>
                                                <p>by <?=$dnews->name?> / <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?></p>
                                            </div>
                                        </div>
                                    </li>
                                </a>
                                <?php
                            }
                            ?>

                        </ul>
                    </div>

                        <!-- <div class="widget widget_instagram">
                            <h4 class="widget-title">
                                Instagram
                            </h4>
                            <ul>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst5.png')}}" alt="instagram"></a></li>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst2.png')}}" alt="instagram"></a></li>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst3.png')}}" alt="instagram"></a></li>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst4.png')}}" alt="instagram"></a></li>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst5.png')}}" alt="instagram"></a></li>
                                <li><a href="#"><img src="{{asset('assets/img/blog/instagram/inst6.png')}}" alt="instagram"></a></li>
                            </ul>
                        </div>
                        <div class="widget widget_tags">
                            <h4 class="widget-title">
                                Tags
                            </h4>
                            <ul>
                                <li><a href="#">branding</a></li>
                                <li><a href="#">identity</a></li>
                                <li><a href="#">design</a></li>
                                <li><a href="#">inspiration</a></li>
                                <li><a href="#">web design</a></li>
                                <li><a href="#">video</a></li>
                                <li><a href="#">photography</a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
                <!--sidebar section end -->

            </div>
        </div>
    </section>
<!--event section end -->
@include('layouts.footer')
<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>