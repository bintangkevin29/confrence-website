@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    foreach ($about as $dabout) {
    }
    ?>
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Speakers <small> &ndash; <?= $dabout->short_title ?></small>
                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>
    <!--page title section end-->

    <!--about section -->
    <!--about section end -->

    <!--speaker section-->
    <section class="pt100">
        <?php
        foreach ($type as $dtype) {
        ?>
            <div class="container">
                <div class="section_title mb50">
                    <h3 class="title">
                        <?= $dtype->desc ?>
                    </h3>
                </div>
            </div>
            <?php
                if($dtype->id == 3){
                    $container = 'container';
                }
                else
                {
                    $container = 'container-fluid';
                }
            ?>
            <div class="<?=$container?>">
                <div class="row justify-content-center no-gutters pb100">
                    <?php
                    $tbc = '';
                    foreach ($committee as $dcommittee) {
                        if ($dcommittee->confirmed == 'unconfirmed') {
                            $tbc = '<small><i>To Be Confirmed</i></small>';
                        } else {
                            $tbc = '';
                        }
                        if ($dcommittee->id_type == $dtype->id) {
                            if ($dcommittee->id_type != 3) {

                    ?>
                                <div class="col-md-3 col-sm-6">
                                    <div class="speaker_box">
                                        <div class="speaker_img">
                                            <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                                            <div class="info_box">
                                                <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5><?= $tbc ?>
                                                <p class="position">{{ $dcommittee->short_desc }}<br></p>
                                            </div>
                                        </div>
                                        <!-- <div class="speaker_social">
                                    <p>
                                        <?= $dcommittee->speakers_longdesc ?>
                                    </p>

                                </div> -->
                                    </div>
                                </div>
                            <?php
                            } else {

                            ?>
                                <div class="col-md-3 col-sm-6" id="invited-speakers">
                                    <div class="speaker_box">
                                        <div class="info_box">
                                            <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5><?= $tbc ?>
                                            <p class="position">{{ $dcommittee->short_desc }}</p>
                                        </div>

                                        <!-- <div class="speaker_social">
                                    <p>
                                        <?= $dcommittee->speakers_longdesc ?>
                                    </p>

                                </div> -->
                                    </div>
                                </div>
                    <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>



    </section>
    <!--speaker section end -->

    @include('layouts.footer')
    <!-- jquery -->
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/js/popper.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{ asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{ asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{ asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{ asset('assets/js/main.js')}}"></script>
</body>

</html>