<?php
foreach ($about as $dabout) {
}
?>
@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Contacts <small> &ndash; <?= $dabout->short_title ?></small>

                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>
    <!--page title section end-->


    <!--contact section -->
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <div class="contact_info">
                        <!-- <ul class="social_list">
                        <li>
                            <a href="#"><i class="ion-social-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-dribbble"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-instagram"></i></a>
                        </li>
                    </ul> -->
                        <?php
                        foreach ($data as $dt) {
                        }
                        ?>
                        <ul class="icon_list pt50">
                            <li>
                                <i class="ion-location"></i>
                                <span>
                                    <?= $dt->address; ?>
                                </span>
                            </li>
                            <li>
                                <i class="ion-ios-telephone"></i>
                                <span>
                                    <?= $dt->phone; ?>
                                </span>
                            </li>
                            <li>
                                <i class="ion-email-unread"></i>
                                <span>
                                    <?= $dt->email; ?>
                                </span>
                            </li>

                            <li>
                                <i class="ion-planet"></i>
                                <span>
                                    <?= $dt->website; ?>
                                </span>
                            </li>

                            <li>
                                <i class="ion-social-facebook"></i>
                                <a class="sosmed" target="_blank" href="https://facebook.com/">
                                    <span>
                                        <?= $dt->facebook; ?>
                                    </span>
                                </a>
                            </li>


                            <li>
                                <i class="ion-social-twitter"></i>
                                <a class="sosmed" target="_blank" href="https://twitter.com/<?= $dt->twitter; ?>">
                                    <span>
                                        @<?= $dt->twitter; ?>
                                    </span>
                                </a>
                            </li>


                            <li>
                                <i class="ion-social-instagram"></i>
                                <a class="sosmed" target="_blank" href="https://instagram.com/<?= $dt->instagram; ?>">
                                    <span>
                                        @<?= $dt->instagram; ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <?php 
                    if (Session::has('success')) {
                        echo '<div class="alert alert-success">' . Session::get("success") . '</div>';
                    }
                    ?>
                    <form method="POST" role="form" class="form-horizontal" action="{{url('conference/contacts_send')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="contact_form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" cols="4" name="messages" rows="4" placeholder="Messages"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-rounded btn-primary">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12 mt70">
                    <!--map -->
                    <iframe style="width:100%; height:450px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.9601948446184!2d98.67298685043195!3d3.5965976512288473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x303131c3f6c8d037%3A0x54771c0e776d21fe!2sJW%20Marriott%20Hotel%20Medan!5e0!3m2!1sen!2sid!4v1578929365669!5m2!1sen!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    <!--map end-->
                </div>
            </div>

        </div>
    </section>
    <!--contact section end -->



    <!--get tickets section -->
    @include('layouts.footer')
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!--map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>