@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    function thousandsCurrencyFormat($num)
    {

        if ($num > 1000) {

            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('K', 'M', 'B', 'T');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;
        }

        return $num;
    }
    ?>
    <?php
    foreach ($about as $dabout) {
    }
    ?>
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Call for Papers <small> &ndash; <?= $dabout->short_title ?></small>
                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>
    <!--page title section end-->


    <!--events section -->
    <section class="pb100 pt100">
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    Call for Papers
                </h3>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <?php
                    foreach ($papers as $dpapers) {
                    ?>
                        <?= $dpapers->desc ?>
                    <?php
                    }
                    ?>
                </div>

            </div>

        </div>
    </section>
    <section class="pb100">
        <div class="container">
            <div class="table-responsive">
                <table class="event_calender table">
                    <thead class="event_title">
                        <tr>
                            <th colspan="4">
                                <i class="ion-ios-calendar-outline"></i>
                                <span>Important Dates</span>
                            </th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($events as $devents) {
                        ?>
                            <tr>
                                <td class="event_date">
                                    <?= Carbon\Carbon::parse($devents->waktu_event)->format('d') ?>
                                    <span><?= Carbon\Carbon::parse($devents->waktu_event)->format('F') ?></span>
                                </td>
                                <td>
                                    <div class="event_place">
                                        <h5><?= $devents->title ?></h5>
                                        <p><?= $devents->event_desc ?></p>
                                    </div>
                                </td>
                                <td>

                                </td>
                                <td class="buy_link">

                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="pb100">
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    VENUE
                </h3>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <?php
                    foreach ($venue as $dvenue) {
                    ?>
                        <?= $dvenue->desc_venue ?>
                    <?php
                    }
                    ?>
                </div>

            </div>

        </div>

        </div>
        </div>
    </section>

    <!--event section end -->
    @include('layouts.footer')
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>