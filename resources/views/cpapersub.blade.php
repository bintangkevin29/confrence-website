<!-- adr -->

@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')

    <?php
    foreach ($about as $dabout) {
    }
    ?>

    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Paper Submission <small> &ndash; <?= $dabout->short_title ?></small>
                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>

    <section class="pb100 pt100">
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    Paper Submission
                </h3>

                <!--  <p>
                    <i class="ionicons ion-alert-circled"></i>
                    <i style="background-color: grey; color: white">
                        is under construction, please come back again later &nbsp
                    </i>
                </p> -->
            </div>


            <div class="row justify-content-left">
                <div class="col-12 col-md-7">
                    <div class="paper_form">
                        <?php
                        if (Session::has('danger')) {
                            echo '<div class="alert alert-danger">' . Session::get("danger") . '</div>';
                        }
                        if (Session::has('success')) {
                            echo '<div class="alert alert-success">' . Session::get("success") . '</div>';
                        }
                        ?>
                        <form method="POST" role="form" class="form-horizontal" action="{{url('conference/cpapersub/add')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                Title
                                <input required type="text" class="form-control" placeholder="" name="title">
                            </div>
                            <div class="form-group">
                                Abstract
                                <textarea required name="abstract" class="form-control" cols="8" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                Author List Name (separated by coma)
                                <input required type="text" class="form-control" name="author">
                            </div>


                            <div class="form-group">
                                Tracks<br>
                                <select required class="form-control-track" name="tracks">
                                    <option selected disabled>
                                        Choose Tracks of Your Paper
                                    </option>

                                    <?php
                                    foreach ($tracks as $dtracks) {
                                        # code...
                                    ?>
                                        <option value="{{$dtracks->id_tracks}}">
                                            <?= $dtracks->title_tracks ?>
                                        </option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                Affiliation
                                <input required type="text" class="form-control" name="affiliation">
                            </div>
                            <div class="form-group">
                                Correspondence Name
                                <input required type="text" class="form-control" name="cor_name">
                            </div>
                            <div class="form-group">
                                Correspondence Email
                                <input required type="email" class="form-control" name="cor_email">
                            </div>

                            <div class="form-group">
                                Abstract <small>(.doc, .docx)</small>
                                <input required type="file" class="form-control" name="file2" accept=".doc, .docx">
                            </div>
                            <div class="form-group">
                                Full Paper <small>(.doc, .docx)</small>
                                <input required type="file" class="form-control" name="file" accept=".doc, .docx">
                            </div>


                            <div class="form-group">
                                Category?<br>
                                <select required class="form-control-track" name="published">
                                    <option selected disabled style="display: none">
                                        Choose
                                    </option>
                                    <option value="1">Proceeding</option>
                                    <option value="2">Non-proceeding</option>
                                </select>
                            </div>


                            <center>
                                @captcha()
                                <br>
                                <div class="form-group">
                                    <input class="btn btn-rounded btn-primary" type="submit" value="Submit">
                                </div>
                            </center>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include('layouts.footer')


    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>