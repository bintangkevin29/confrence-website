@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <!--cover section slider -->
    <section id="home" class="home-cover">
        <div class="cover_slider owl-carousel owl-theme">
            <div class="cover_item" style="background: url('assets/img/bg/expo1.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    MICES Expo
                                    <h3 class="cover-title">
                                        2020
                                    </h3>
                                    <p class="cover-date">
                                        JW Marriott, Medan
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_item" style="background: url('assets/img/bg/expo2.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    MICES Expo
                                    <h3 class="cover-title">
                                        2020
                                    </h3>
                                    <p class="cover-date">
                                        JW Marriott, Medan
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Background
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <p>
                        Today we are looking for solutions to ensure a reliable and environmentally friendly energy supply. The need for a balanced National Energy Plan is very important to ensure economic vitality and energy security.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <p>
                        A country needs to provide a primary energy source and ensure its availability and sustainability, both from fossil resources and renewable resources at the lowest possible cost without reducing environmental quality.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <p>
                        As part of the contribution of the <b>Bandung Institute of Technology (ITB)</b> in commemoration of <b>ITB's 100th annivarsarry,</b> the ITB Alulmni Assiciation of North Sumatra (IA-ITB Sumatera Utara), in collaboration with ITB and the University of Sumatera Utara (USU) held an <b>exhibiton</b> on <b>Energy Security.</b>
                    </p>
                </div>

                <div class="col-12 col-md-6">
                    <p>
                        The exhibiton will be focused to show on various of organization that will contribute toward the energy of Indonesia: <b>education, equipment, and instrument of laboratory, Institution of research and LSM, and industry of Indonesia and foreign.</b>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Purpose
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <p>
                        Expo will be conducted in two days, parallel with the conference. The exhibiton will be focused to display a range of organization contributing toward Indonesia energy sector: education, laboratory equipment and instrument, research institute and government NGO, and industries and aboard.
                    </p>
                </div>
            </div>

            <div class="row justify-content-center mt30">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="icon_box_one">
                        <i class="lnr lnr-mic"></i>
                        <div class="content">
                            <h4>To facilitate </h4>
                            <p>
                                the sharing of cutting-edge findings, knowledge and experience among researchers, academicians, governments, private-companies and NGOs; both from indonesia and aboard, related to sustainable energy security in Indonesia and aboard.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="icon_box_one">
                        <i class="lnr lnr-rocket"></i>
                        <div class="content">
                            <h4>To build</h4>
                            <p>
                                and strengthen networks of colleagues and cooperation among researchers academicians, governments, private-companies and NGOs in the sustainable energy field in Indonesia.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="icon_box_one">
                        <i class="lnr lnr-bullhorn"></i>
                        <div class="content">
                            <h4>To facilitate</h4>
                            <p>
                                input for government in order to build better polciies in sustainable energy in Indonesia.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="icon_box_one">
                        <i class="lnr lnr-clock"></i>
                        <div class="content">
                            <h4>To promote</h4>
                            <p>
                                the product and instrument from national and international industries related to energy sector in Indonesia.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="bg-gray pb100 pt100">
        <div class="row">
            <div class="col-12 text-center">
                <h2>1K (1,000) OF VISITORS</h2>
                <h5>Involved:</h5>
                <h4>GOVERNMENT</h4>
                <h4>COMPANIES</h4>
                <h4>RESEARCHES</h4>
                <h4>HEADMAN</h4>
                <h4>UNIVERSITIES / LECTURERS / STUDENTS</h4>
                <h4>SENIOR HIGH SCHOOL STUDENTS / EQUALS</h4>
                <h4>PUBLIC</h4>
            </div>
        </div>
    </section>

    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6 justify-content-center">
                    <div class="icon_box_two">
                        <i class="ion-ios-calendar-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                DATE
                            </h5>
                            <p>
                                April 15 - 16, 2020
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="ion-ios-location-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                location
                            </h5>
                            <p>
                                JW. Marriott Hotel, Medan
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="pb100">
        <div class="container">
            <div class="table-responsive">
                <table class="event_calender table">
                    <thead class="event_title">
                        <tr>
                            <th colspan="2">
                                <i class="ion-ios-calendar-outline"></i>
                                <span>next events calendar</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="event_date">
                                15
                                <span>April 2020</span>
                                <span>10.00 AM - 09.00 PM</span>
                            </td>
                            <td>
                                <div class="event_place">
                                    <h5>Talkshow</h5>
                                    <h5>Sosialization</h5>
                                    <h5>Promotion Product on Stage</h5>
                                    <h5>Entertainment</h5>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="event_date">
                                16
                                <span>April 2020</span>
                                <span>10.00 AM - 09.00 PM</span>
                            </td>
                            <td>
                                <div class="event_place">
                                    <h5>Talkshow</h5>
                                    <h5>Sosialization</h5>
                                    <h5>Promotion Product on Stage</h5>
                                    <h5>Entertainment</h5>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Publication and Promotion
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <ol type="1">
                        <li>Publication of the programme
                            <ol type="a">
                                <li>Electronic Media
                                    <ul>
                                        <li>Social Media flyer</li>
                                        <li>Radio</li>
                                    </ul>
                                </li>
                                <li>Outdoor Media
                                    <ul>
                                        <li>Billboard</li>
                                        <li>Banner 5 x 1 m</li>
                                        <li>Flag Banner 3 x 0.8 m</li>
                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <li>Publication of sponsorship
                            <ol type="a">
                                <li>Billboard</li>
                                <li>Banner</li>
                                <li>Flag Banner</li>
                                <li>Radio</li>
                                <li>Social Media</li>
                            </ol>
                        </li>
                    </ol>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <img src="{{asset('assets/img/expo/reklame1.png')}}" alt="" srcset="">
                        </div>
                        <div class="col-6">
                            <img src="{{asset('assets/img/expo/reklame2.png')}}" alt="" srcset="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    General Layout
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <ul type="1">
                        <li>1st Floor</li>
                    </ul>
                </div>
                <div class="col-12 col-md-12 text-center">
                    <img src="{{asset('assets/img/expo/denah1.png')}}" alt="" srcset="">
                </div>
                <div class="col-12 col-md-12 pt100">
                    <ul type="2">
                        <li>2nd Floor</li>
                    </ul>
                </div>
                <div class="col-12 col-md-12 text-center">
                    <img src="{{asset('assets/img/expo/denah2.png')}}" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Sponsorship Package
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <div class="table-responsive">
                        <table class="event_calender table">
                            <thead class="event_title">
                                <tr>
                                    <th>
                                        Green Packages &ndash; IDR 40.000.000
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left">
                                        <ol type="1">
                                            <li>Booth space 6 x 4 m and facilites</li>
                                            <li>Logo of premier sponsor company (Green and Energy) at flag banner as many as 30 pcs</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on banner as many as 10 pcs</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on Billboard at Jl. S Parman and KNO Medan</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on backdrop stage, gate, and receptionist</li>
                                            <li>Company name on flyer media social</li>
                                            <li>Company name on Radio advertisement media IR Radio</li>
                                            <li>Free conference participants tickets for 4 persons</li>
                                        </ol>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-md-12 pt100">
                    <div class="table-responsive">
                        <table class="event_calender table">
                            <thead class="event_title">
                                <tr>
                                    <th>
                                        Energy Packages &ndash; IDR 25.000.000
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left">
                                        <ol type="1">
                                            <li>Booth space 6 x 4 m or 3 x 4 m and facilites</li>
                                            <li>Logo of premier sponsor company (Green and Energy) at flag banner as many as 30 pcs</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on banner as many as 10 pcs</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on Billboard at Jl. S Parman and KNO Medan</li>
                                            <li>Logo of premier sponsor company (Green and Energy) on backdrop stage, gate, and receptionist</li>
                                            <li>Company name on flyer media social</li>
                                            <li>Company name on Radio advertisement media IR Radio</li>
                                            <li>Free conference participants tickets for 2 persons</li>
                                        </ol>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-md-12 pt100">
                    <div class="table-responsive">
                        <table class="event_calender table">
                            <thead class="event_title">
                                <tr>
                                    <th class="text-center">
                                        Booth Type
                                    </th>
                                    <th class="text-center">Size</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Facilites</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="event_date">
                                        Major
                                    </td>
                                    <td>6 x 4 m</td>
                                    <td>IDR 28.000.000</td>
                                    <td class="text-left">
                                        <ul>
                                            <li>Without partition</li>
                                            <li>2 tables and 4 chairs</li>
                                            <li>Mcb 4 amp</li>
                                            <li>Electric socket as 4 hole</li>
                                            <li>TL Lamp 4 Units</li>
                                            <li>2 trash bag</li>
                                            <li>Fascia Name</li>
                                            <li>ID Card Exhibition</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="event_date">
                                        Madya
                                    </td>
                                    <td>6 x 2 m</td>
                                    <td>IDR 15.000.000</td>
                                    <td class="text-left">
                                        <ul>
                                            <li>Without partition</li>
                                            <li>2 tables and 4 chairs</li>
                                            <li>Mcb 4 amp</li>
                                            <li>Electric socket as 4 hole</li>
                                            <li>TL Lamp 4 Units</li>
                                            <li>2 trash bag</li>
                                            <li>Fascia Name</li>
                                            <li>ID Card Exhibition</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="event_date">
                                        Standard
                                    </td>
                                    <td>3 x 2 m</td>
                                    <td>IDR 8.000.000</td>
                                    <td class="text-left">
                                        <ul>
                                            <li>1 table and 2 chairs</li>
                                            <li>Mcb 2 amp</li>
                                            <li>Electric socket as 4 hole</li>
                                            <li>TL Lamp 4 Units</li>
                                            <li>1 trash bag</li>
                                            <li>Fascia Name</li>
                                            <li>ID Card Exhibition</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="event_date">
                                        Regular
                                    </td>
                                    <td>2 x 2 m</td>
                                    <td>IDR 4.000.000</td>
                                    <td class="text-left">
                                        <ul>
                                            <li>1 table and 2 chairs</li>
                                            <li>Mcb 2 amp</li>
                                            <li>Electric socket as 4 hole</li>
                                            <li>TL Lamp 4 Units</li>
                                            <li>1 trash bag</li>
                                            <li>Fascia Name</li>
                                            <li>ID Card Exhibition</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Booth
                </h3>
            </div>
            <div class="row justify-content-center"> 
                <div class="col-12 col-md-12 text-center">
                    <img src="{{asset('assets/img/expo/booth1.png')}}" alt="" srcset="">
                </div> 
                <div class="col-12 col-md-12 text-center">
                    <img src="{{asset('assets/img/expo/booth2.png')}}" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>
    <section class="pb100 pt100" id="register">
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    Booth Registration
                </h3>

                <!--  <p>
                    <i class="ionicons ion-alert-circled"></i>
                    <i style="background-color: grey; color: white">
                        is under construction, please come back again later &nbsp
                    </i>
                </p> -->
            </div>


            <div class="row justify-content-left">
                <div class="col-12 col-md-7">
                    <div class="paper_form">
                        <?php
                        if (Session::has('danger')) {
                            echo '<div class="alert alert-danger">' . Session::get("danger") . '</div>';
                        }
                        if (Session::has('success')) {
                            echo '<div class="alert alert-success">' . Session::get("success") . '</div>';
                        }
                        ?>
                        <form method="POST" role="form" class="form-horizontal" action="{{url('expo/add')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                Registrant Name
                                <input required type="text" class="form-control" placeholder="" name="name">
                            </div>
                            <div class="form-group">
                                Registrant Email
                                <input required type="email" class="form-control" placeholder="" name="email">
                            </div>
                            <div class="form-group">
                                Name of Company / Association / Personal
                                <input required type="text" class="form-control" placeholder="" name="company">
                            </div>

                            <div class="form-group">
                                Address
                                <input required type="text" class="form-control" placeholder="" name="address">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="booth" value="1" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Booth
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="sponsorship" value="1" id="defaultCheck2">
                                    <label class="form-check-label" for="defaultCheck2">
                                        Sponsorship
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="event" value="1" id="defaultCheck2">
                                    <label class="form-check-label" for="defaultCheck2">
                                        Event on Stage <small>(Talk Show / Interactive Dialog / Socialization / Product Placement)</small>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                ID Card Number
                                <input required type="text" class="form-control" placeholder="" name="idCard">
                            </div>
                            <div class="form-group">
                                Phone Number
                                <input required type="text" class="form-control" placeholder="" name="phoneNumber">
                            </div>
                            <div class="form-group">
                                Product
                                <textarea required name="product" class="form-control" cols="8" rows="10"></textarea>
                                <small><i> Indeed that Company / Association / Personal, we intend to follow "EXPO 100 Years ITB" at Hotel JW Marriott Medan</i></small>
                            </div>
                            <div class="form-group">
                                Price of Booth (IDR):
                                <input required type="number" class="form-control" placeholder="" name="price">
                            </div>

                            <center>
                                @captcha()
                                <br>
                                <div class="form-group">
                                    <input class="btn btn-rounded btn-primary" type="submit" value="Submit">
                                </div>
                            </center>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--cover section slider end -->


    <!--brands section end-->
    @include('layouts.footer')
    <!--footer end -->
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
    <script type="text/javascript">
        $(".countdown")
        $(this).html(
        event.strftime('<div>%D <span>Days</span></div>  <div>%H<span>Hours</span></div> <div>%M<span>Minutes</span></div> <div>%S<span>Seconds</span></div>')
        );
        });
    </script>
</body>

</html>