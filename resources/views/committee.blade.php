@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/bg-img.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Committee
                        </h3>
                    </div>
                </div>
            </div>

            <div class="breadcrumbs">
                <ul>
                    <li><a href="{{ url('/')}}">Home</a>   |  </li>
                    <li><span>Committee</span></li>
                </ul>
            </div>
        </div>
    </section>
    <!--page title section end-->

    <!--about section -->
    <section class="pt100 pb50">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Our Committee
                </h3>
            </div>
        </div>
    </section>
    <!--about section end -->

    <!--speaker section-->
    <section class="pb80">
        <div class="container">
            <div class="row justify-content-center"> 
                <?php
                $jabatan = '';
                $check = '';
                $i=0;
                $buka = '';
                $tutup = '';

                foreach ($committee as $dcommittee) 
                {
                    
                    if($check == $dcommittee->short_desc)
                    {
                        $jabatan = '';
                        $buka = '';
                        $tutup = '';
                    }
                    else
                    {
                        $jabatan = $dcommittee->short_desc;
                        $buka = '<div class="col-md-4 col-sm-6 pb10">';
                        $tutup = '</div>';
                    } 
                    if($i == 0)
                    {
                        $buka = '<div class="col-md-4 col-sm-6 pb10">';
                        $tutup = '';
                    }
                    ?>
                    <?= $tutup?>        
                    <?= $buka?>
                    <b>{{ $jabatan }}</b><br>
                    {{ $dcommittee->name_of_speakers }}
                    
                <?php
                $check = $dcommittee->short_desc;
                $i++;
            }
            ?>

                    <!-- <div class="speaker_box">
                        <div class="speaker_img">
                            <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                            <div class="info_box">
                                <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5>
                                <p class="position">{{ $dcommittee->short_desc }}</p>
                            </div>
                        </div>
                        <div class="speaker_social">
                            <p>
                                <?= $dcommittee->speakers_longdesc ?>
                            </p>

                        </div>
                    </div>
                -->        </div>
            </div> 

        </div>

    </section>
    <!--speaker section end -->

    @include('layouts.footer') 
    <!-- jquery -->
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/js/popper.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{ asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{ asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{ asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{ asset('assets/js/main.js')}}"></script>
</body>
</html>