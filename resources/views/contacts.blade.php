@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')

    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/bg-img.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            contact us
                        </h3>
                    </div>
                </div>
            </div>

            <div class="breadcrumbs">
                <ul>
                    <li><a href="{{ url('/')}}">Home</a> | </li>
                    <li><span>Contact</span></li>
                </ul>
            </div>
        </div>
    </section>
    <!--page title section end-->


    <!--contact section -->
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <div class="contact_info">
                        <!-- <ul class="social_list">
                        <li>
                            <a href="#"><i class="ion-social-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-dribbble"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-instagram"></i></a>
                        </li>
                    </ul> -->
                        <?php
                        foreach ($data as $dt) {
                        }
                        ?>
                        <ul class="icon_list pt50">
                            <li>
                                <i class="ion-location"></i>
                                <span>
                                    <?= $dt->address; ?>
                                </span>
                            </li>
                            <li>
                                <i class="ion-ios-telephone"></i>
                                <span>
                                    <?= $dt->phone; ?>
                                </span>
                            </li>
                            <li>
                                <i class="ion-email-unread"></i>
                                <span>
                                    <?= $dt->email; ?>
                                </span>
                            </li>

                            <li>
                                <i class="ion-planet"></i>
                                <span>
                                    <?= $dt->website; ?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="contact_form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="subject">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" cols="4" rows="4" placeholder="massage"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-rounded btn-primary">send massage</button>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt70">
                    <!--map -->
                    <iframe style="width:100%; height:450px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.9601948446184!2d98.67298685043195!3d3.5965976512288473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x303131c3f6c8d037%3A0x54771c0e776d21fe!2sJW%20Marriott%20Hotel%20Medan!5e0!3m2!1sen!2sid!4v1578929365669!5m2!1sen!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    <!--map end-->
                </div>
            </div>

        </div>
    </section>
    <!--contact section end -->



    <!--get tickets section -->
    @include('layouts.footer')
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!--map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>