@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <!--cover section slider -->
    <section id="home" class="home-cover">
        <div class="cover_slider owl-carousel owl-theme">


            <?php foreach ($covertitle as $covtitle) {
                $title1 = $covtitle->cover_title;
                $title2 = $covtitle->fokus_utama;
                $title3 = $covtitle->cover_subtitle;
            } ?>

            <div class="cover_item" style="background: url('assets/img/bg/slider.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    <?= $title2 ?></strong>
                                <h3 class="cover-title">
                                    <?= $title1 ?>
                                </h3>
                                <p class="cover-date">
                                    <?= $title3 ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_item" style="background: url('assets/img/bg/slider2.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    <?= $title2 ?></strong>
                                <h3 class="cover-title">
                                    <?= $title1 ?>
                                </h3>
                                <p class="cover-date">
                                    <?= $title3 ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_item" style="background: url('assets/img/bg/slider3.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    <?= $title2 ?></strong>
                                <h3 class="cover-title">
                                    <?= $title1 ?>
                                </h3>
                                <p class="cover-date">
                                    <?= $title3 ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_item" style="background: url('assets/img/bg/slider4.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    <?= $title2 ?></strong>
                                <h3 class="cover-title">
                                    <?= $title1 ?>
                                </h3>
                                <p class="cover-date">
                                    <?= $title3 ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_item" style="background: url('assets/img/bg/slider5.png');">
                <div class="slider_content">
                    <div class="slider-content-inner">
                        <div class="container">
                            <div class="slider-content-left">
                                <strong class="cover-xl-text">
                                    <?= $title2 ?></strong>
                                <h3 class="cover-title">
                                    <?= $title1 ?>
                                </h3>
                                <p class="cover-date">
                                    <?= $title3 ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--cover section slider end -->
    <?php
    foreach ($about as $dabout) {
    }
    ?>

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title" style="color: red;">
                    POSTPONEMENT ANNOUNCEMENT OF MICES 2020
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12"><b>
                    
Dear All  MICES Speakers, Presenters and Participants, <br><br>

During the last 2-3 weeks, together we have heard news about Corona Epidemic that has become a serious issue, both internationally and nationally. Further, many governments and universities have also provided anticipation warning regarding the potential of corona epidemic spread in public forum.

Considering the facts and several things below:<br>
• Insights and inputs from partners and colleagues inside and outside the country<br>
• Corona epidemic situation currently in Indonesia that is expected to increase but has not reached the peak, hence it's presumed that the peak situation will happen in the next 1-2 months <br>
• All these perplexity is presumed to subside in Indonesia after June-July 2020<br><br>

Therefore, with a heavy heart, we are sorry to announce that MICES 2020 would be <b> postponed to August 2020 </b>. The new dates will be announced later in the next week. <br><br>

For further information, please contact our committee via WA: (+62) 822.9607.3952, or email: info@100tahunitb-iaitbsumut.info.<br><br>

We are sorry for this inconvenience, and thank you for your kind understanding.<br><br>

Warm regards,<br>
Zaid Nasution, Ph. D <br>
Chairman of MICES 2020 Organizing Committee <br></b>
                </div>
            </div>

            <!--event features-->
            <!--event features end-->
        </div>
    </section>


    <!--event info -->
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="ion-ios-calendar-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                DATE
                            </h5>
                            <p>
                                <?= Carbon\Carbon::parse($dabout->countdown)->toFormattedDateString() ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="ion-ios-location-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                location
                            </h5>
                            <p>
                                {{ $dabout->location }}
                            </p>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-person-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            speakers
                        </h5>
                        <p>
                            Coming Soon
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-pricetag-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            tikets
                        </h5>
                        <p>
                            $65 early bird
                        </p>
                    </div>
                </div>
            </div> -->
            </div>
        </div>
    </section>
    <!--event info end -->


    <!--event countdown -->
    <section class="bg-img pt70 pb70" style="background-image: url('assets/img/bg/bg-img.png');">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <h4 class="mb30 text-center color-light">Counter until the big event</h4>
                    <div class="countdown"></div>
                </div>
            </div>
        </div>
    </section>
    <!--event count down end-->


    <!--about the event -->
    <section class="pt60 pb50">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    About the event
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12" id="events">
                    <!--                 <p>
                    {{$dabout->about}}
                </p> -->
                </div>
            </div>

            <!--event features-->
            <!--event features end-->
        </div>
    </section>
    <!--about the event end -->

    <section class="pb100">
        <div class="container">
            <div class="table-responsive">
                <table class="event_calender table">
                    <thead class="event_title">
                        <tr>
                            <th colspan="4">
                                <i class="ion-ios-calendar-outline"></i>
                                <span>next events calendar</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($events as $devents) {
                        ?>
                            <tr>
                                <td class="event_date">
                                    <?= Carbon\Carbon::parse($devents->waktu_event)->format('d') ?>
                                    <span><?= Carbon\Carbon::parse($devents->waktu_event)->format('F') ?></span>
                                </td>
                                <td>
                                    <div class="event_place">
                                        <h5 style="margin-bottom: 2px" ;><?= $devents->title ?></h5>
                                        <p><?= $devents->event_desc ?></p>
                                    </div>
                                </td>
                                <td>

                                </td>
                                <td class="buy_link">

                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <!--speaker section-->
    <section class="pb80">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    Committee
                </h3>
            </div>
            <div class="row justify-content-center">
                <?php
                $jabatan = '';
                $check = '';
                $i = 0;
                $buka = '';
                $Close = '';

                foreach ($committee as $dcommittee) {

                    if ($check == $dcommittee->short_desc) {
                        $jabatan = '';
                        $buka = '';
                        $Close = '';
                    } else {
                        $jabatan = $dcommittee->short_desc;
                        $buka = '<div class="col-md-4 col-sm-6 pb10">';
                        $Close = '</div>';
                    }
                    if ($i == 0) {
                        $buka = '<div class="col-md-4 col-sm-6 pb10">';
                        $Close = '';
                    }
                ?>
                    <?= $Close ?>
                    <?= $buka ?>
                    <b>{{ $jabatan }}</b><br>
                    {{ $dcommittee->name_of_speakers }}

                <?php
                    $check = $dcommittee->short_desc;
                    $i++;
                }
                ?>

                <!-- <div class="speaker_box">
                        <div class="speaker_img">
                            <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                            <div class="info_box">
                                <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5>
                                <p class="position">{{ $dcommittee->short_desc }}</p>
                            </div>
                        </div>
                        <div class="speaker_social">
                            <p>
                                <?= $dcommittee->speakers_longdesc ?>
                            </p>

                        </div>
                    </div>
                -->
            </div>
        </div>

        </div>

    </section>
    <!--speaker section end -->


    <!--brands section -->
    <section class="bg-gray pt100 pb100">
        <div class="container">
            <div class="brand_carousel owl-carousel pb100">
                <?php
                foreach ($vip as $dvip) {
                ?>
                    <div class="brand_item text-center">
                        <img src="{{asset('assets/img/brands/')}}{{'/'.$dvip->img_vip}}" alt="<?= $dvip->name_vip ?>">
                    </div>
                <?php
                }
                ?>


            </div>
            <div class="row">
                <div class="section_title">
                    <h3 class="title">
                        Live Streaming Coming Soon
                    </h3>
                </div>
                <div class="col-12 pb100">
                    <div class="col-12">
                        <iframe width="100%" height="500" src="https://www.youtube.com/embed/AMPbfkvlJuE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>
            <div class="section_title">
                <h3 class="title">
                    Printed Media
                </h3>
            </div>
            <div class="col-12 pb100">
                <div class="row">
                    <div class="col-6 text-center">
                        <a href="#" data-target="#modalPoster" data-toggle="modal">
                            <img src="{{asset('assets/img/poster-01_compressed.jpg')}}" style="height:400px;" alt="" srcset="">
                        </a>
                        <br>
                        <small>Click to enlarge</small>
                    </div>
                    <div class="col-6 text-center">
                        <a href="#" data-target="#modalFlyer" data-toggle="modal">
                            <img src="{{asset('assets/img/flyer.jpg')}}" style="height:400px;" alt="" srcset="">
                        </a>
                        <br>
                        <small>Click to enlarge</small>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!--map -->
                <iframe style="width:100%; height:450px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.9601948446184!2d98.67298685043195!3d3.5965976512288473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x303131c3f6c8d037%3A0x54771c0e776d21fe!2sJW%20Marriott%20Hotel%20Medan!5e0!3m2!1sen!2sid!4v1578929365669!5m2!1sen!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                <!--map end-->
            </div>
        </div>
        </div>
    </section>
    <!--brands section end-->
    @include('layouts.footer')
    <!--footer end -->
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
    <script type="text/javascript">
        $(".countdown")
            .countdown("<?= Carbon\Carbon::parse($dabout->countdown)->toDateString() ?>", function(event) {
                $(this).html(
                    event.strftime('<div>%D <span>Days</span></div>  <div>%H<span>Hours</span></div> <div>%M<span>Minutes</span></div> <div>%S<span>Seconds</span></div>')
                );
            });
    </script>
</body>

</html>

<div id="modalFlyer" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Flyer</h4>
            </div>
            <div class="modal-body">
                <img src="{{asset('assets/img/modal/flyer.jpg')}}" alt="" srcset="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modalPoster" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Poster</h4>
            </div>
            <div class="modal-body">
                <img src="{{asset('assets/img/modal/poster.jpg')}}" alt="" srcset="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>