@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    foreach ($about as $dabout) {
    }

    foreach ($pricing as $dpricing) {
    }

    function thousandsCurrencyFormat($num)
    {

        if ($num > 1000) {

            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('K', 'M', 'B', 'T');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;
        }

        return $num;
    }
    ?>

    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            <?= $dabout->title ?> &ndash; <?= $dabout->short_title ?>
                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>
    <!--cover section slider end -->

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title" style="color: red;">
                    POSTPONEMENT ANNOUNCEMENT OF MICES 2020
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12"><b>
                    
Dear All  MICES Speakers, Presenters and Participants, <br><br>

During the last 2-3 weeks, together we have heard news about Corona Epidemic that has become a serious issue, both internationally and nationally. Further, many governments and universities have also provided anticipation warning regarding the potential of corona epidemic spread in public forum.

Considering the facts and several things below:<br>
• Insights and inputs from partners and colleagues inside and outside the country<br>
• Corona epidemic situation currently in Indonesia that is expected to increase but has not reached the peak, hence it's presumed that the peak situation will happen in the next 1-2 months <br>
• All these perplexity is presumed to subside in Indonesia after June-July 2020<br><br>

Therefore, with a heavy heart, we are sorry to announce that MICES 2020 would be <b> postponed to August 2020 </b>. The new dates will be announced later in the next week. <br><br>

For further information, please contact our committee via WA: (+62) 822.9607.3952, or email: info@100tahunitb-iaitbsumut.info.<br><br>

We are sorry for this inconvenience, and thank you for your kind understanding.<br><br>

Warm regards,<br>
Zaid Nasution, Ph. D <br>
Chairman of MICES 2020 Organizing Committee <br></b>
                </div>
            </div>

            <!--event features-->
            <!--event features end-->
        </div>
    </section>

    <!--event info -->

    <section class="pt100 pb100">
        <div class="container">
            <div class="section_title">
                <h3 class="title">
                    About the Conference
                </h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <?= htmlspecialchars_decode($dabout->about) ?>
                </div>
            </div>

            <!--event features-->
            <!--event features end-->
        </div>
    </section>

    <section class="pt100">
        <?php
        foreach ($type as $dtype) {
        ?>
            <div class="container">
                <div class="section_title mb50">
                    <h3 class="title">
                        <?= $dtype->desc ?>
                    </h3>
                </div>
            </div>
            <?php
            if ($dtype->id == 3) {
                $container = 'container';
            } else {
                $container = 'container-fluid';
            }
            ?>
            <div class="<?= $container ?>">
                <div class="row justify-content-center no-gutters pb100">
                    <?php
                    $tbc = '';
                    foreach ($committee as $dcommittee) {
                        if ($dcommittee->confirmed == 'unconfirmed') {
                            $tbc = '<small><i>To Be Confirmed</i></small>';
                        } else {
                            $tbc = '';
                        }
                        if ($dcommittee->id_type == $dtype->id) {
                            if ($dcommittee->id_type != 3) {

                    ?>
                                <div class="col-md-3 col-sm-6">
                                    <div class="speaker_box">
                                        <div class="speaker_img">
                                            <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                                            <div class="info_box">
                                                <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5><?= $tbc ?>
                                                <p class="position">{{ $dcommittee->short_desc }}<br></p>
                                            </div>
                                        </div>
                                        <!-- <div class="speaker_social">
                                    <p>
                                        <?= $dcommittee->speakers_longdesc ?>
                                    </p>

                                </div> -->
                                    </div>
                                </div>
                            <?php
                            } else {

                            ?>
                                <div class="col-md-3 col-sm-6" id="invited-speakers">
                                    <div class="speaker_box">
                                        <div class="info_box">
                                            <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5><?= $tbc ?>
                                            <p class="position">{{ $dcommittee->short_desc }}</p>
                                        </div>

                                        <!-- <div class="speaker_social">
                                    <p>
                                        <?= $dcommittee->speakers_longdesc ?>
                                    </p>

                                </div> -->
                                    </div>
                                </div>
                    <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>



    </section>

    <!--event count down end-->
    <!--event calender-->
    <section class="">
        <div class="container">
            <div class="table-responsive">
                <table class="event_calender table">
                    <thead class="event_title">
                        <tr>
                            <th colspan="4">
                                <i class="ion-ios-calendar-outline"></i>
                                <span>Important Dates</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($events as $devents) {
                        ?>
                            <tr>
                                <td class="event_date">
                                    <?= Carbon\Carbon::parse($devents->waktu_event)->format('d') ?>
                                    <span><?= Carbon\Carbon::parse($devents->waktu_event)->format('F') ?></span>
                                </td>
                                <td>
                                    <div class="event_place">
                                        <h5><?= $devents->title ?></h5>
                                        <p><?= $devents->event_desc ?></p>
                                    </div>
                                </td>
                                <td>

                                </td>
                                <td class="buy_link">

                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6 col-md-3">
                    <div class="icon_box_two">
                        <i class="ion-ios-calendar-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                DATE
                            </h5>
                            <p>
                                <?= Carbon\Carbon::parse($dabout->countdown)->toFormattedDateString() ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-6 col-md-3">
                    <div class="icon_box_two">
                        <i class="ion-ios-location-outline"></i>
                        <div class="content">
                            <h5 class="box_title">
                                location
                            </h5>
                            <p>
                                {{ $dabout->location }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--event info end -->


    <!--event countdown -->
    <section class="bg-img pt70 pb70" style="background-image: url('assets/img/bg/bg-img.png');">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <h4 class="mb30 text-center color-light">Counter until the big event</h4>
                    <div class="countdown"></div>
                </div>
            </div>
        </div>
    </section>
    <!--event calender end -->

    <!--brands section -->
    <!-- <section class="bg-gray pt100 pb100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                our partners
            </h3>
        </div>
        <div class="brand_carousel owl-carousel">
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b1.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b2.png')}}" alt="brand">
            </div>

            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b3.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b4.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b5.png')}}" alt="brand">
            </div>
        </div>
    </div>
</section> -->
    <!--brands section end-->
    @include('layouts.footer')
    <!--footer end -->
    <!-- jquery -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script type="text/javascript">
        $(".countdown")
            .countdown("<?= Carbon\Carbon::parse($dabout->countdown)->toDateString() ?>", function(event) {
                $(this).html(
                    event.strftime('<div>%D <span>Days</span></div>  <div>%H<span>Hours</span></div> <div>%M<span>Minutes</span></div> <div>%S<span>Seconds</span></div>')
                );
            });
    </script>
</body>

</html>