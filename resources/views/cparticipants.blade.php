<!-- adr -->

@include('layouts.head')

<body>
    @include('layouts.loader')
    @include('layouts.header')

    <?php
    foreach ($about as $dabout) {
    }
    ?>

    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Participant Registration <small> &ndash; <?= $dabout->short_title ?></small>
                        </h3>
                    </div>
                </div>
            </div>

            @include('layouts.conference_nav')

        </div>
    </section>

    <section class="pb100 pt100">
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    Participant Registration
                </h3>

                <!--  <p>
                    <i class="ionicons ion-alert-circled"></i>
                    <i style="background-color: grey; color: white">
                        is under construction, please come back again later &nbsp
                    </i>
                </p> -->
            </div>


            <div class="row justify-content-left">
                <div class="col-12 col-md-7">
                    <div class="paper_form">
                        <?php
                        if (Session::has('danger')) {
                            echo '<div class="alert alert-danger">' . Session::get("danger") . '</div>';
                        }
                        if (Session::has('success')) {
                            echo '<div class="alert alert-success">' . Session::get("success") . '</div>';
                        }
                        ?>
                        <form method="POST" role="form" class="form-horizontal" action="{{url('conference/cpapersub/addParticipants')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                Correspondence Name
                                <input required type="text" class="form-control" name="name">
                            </div>
                            <div class="form-group">
                                Correspondence Email
                                <input required type="email" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                Phone Number
                                <input required type="text" class="form-control" name="number">
                            </div>
                            <div class="form-group">
                                Affiliation
                                <input required type="text" class="form-control" name="affiliation">
                            </div>
                            <div class="form-group">
                                Affiliation Type<br>
                                <select required class="form-control-track" name="aff_type">
                                    <option selected disabled style="display: none">
                                        Choose Affiliation
                                    </option>
                                    <option value="Private Organization / Company">Private Organization / Company</option>
                                    <option value="Academic">Academic</option>
                                    <option value="Government">Government</option>
                                </select>
                            </div>

                            <div class="form-group">
                                What tracks are you interested in?<br>
                                <select id="select" required class="form-control-track" name="tracks">
                                    <option selected disabled style="display: none">
                                        Choose Tracks
                                    </option>

                                    <?php
                                    foreach ($tracks as $dtracks) {
                                        # code...
                                    ?>
                                        <option value="{{$dtracks->id_tracks}}">
                                            <?= $dtracks->title_tracks ?>
                                        </option>

                                    <?php
                                    }
                                    ?>
                                    <option value="other">Other...</option>
                                </select>
                            </div>
                            <div id="idea" class="form-group">
                                Would you mind sharing your idea for conference track?
                                <input type="text" class="form-control" name="idea">
                            </div>
                            <center>
                                @captcha()
                                <br>
                                <div class="form-group">
                                    <input class="btn btn-rounded btn-primary" type="submit" value="Submit">
                                </div>
                            </center>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include('layouts.footer')


    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#idea').hide(100);
            $('#select').change(function() {
                var selectedOption = $(this).val();
                if (selectedOption == 'other') {
                    $('#idea').show(100);
                } else {
                    $('#idea').hide(100);

                }
            })
        })
    </script>
</body>

</html>