@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<!--page title section-->
<section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/bg-img.png')}}">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="inner_cover_content">
                    <h3>
                       Events
                    </h3>
                </div>
            </div>
        </div>

        <div class="breadcrumbs">
            <ul>
                <li><a href="{{ url('/')}}">Home</a>  |   </li>
                <li><span>Events</span></li>
            </ul>
        </div>
    </div>
</section>
<!--page title section end-->


<!--events section -->
<section class="pt100 pb100">
    <div class="container">

        <div class="event_box">
            <img src="{{asset('assets/img/events/event1.png')}}" alt="event">
            <div class="event_info">
                <div class="event_title">
                    Los Angeles Event
                </div>
                <div class="speakers">
                    <strong>Speakers</strong>
                    <span>Maria Smith,  Gabriel Hernandez,  Michael Williams</span>
                </div>
                <div class="event_date">
                    February 14, 2018
                </div>
            </div>
            <div class="event_word">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                </div>
            </div>
            <a href="#" class="readmore_btn">read more</a>
        </div>

        <div class="event_box">
            <img src="{{asset('assets/img/events/event2.png')}}" alt="event">
            <div class="event_info">
                <div class="event_title">
                    san francisco Event
                </div>
                <div class="speakers">
                    <strong>Speakers</strong>
                    <span>Maria Smith,  Gabriel Hernandez,  Michael Williams</span>
                </div>
                <div class="event_date">
                    February 14, 2018
                </div>
            </div>
            <div class="event_word">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                </div>
            </div>
            <a href="#" class="readmore_btn">read more</a>
        </div>

        <div class="event_box">
            <img src="{{asset('assets/img/events/event3.png')}}" alt="event">
            <div class="event_info">
                <h5 class="event_title">
                    New york Event
                </h5>
                <div class="speakers">
                    <strong>Speakers</strong>
                    <span>Maria Smith,  Gabriel Hernandez,  Michael Williams</span>
                </div>
                <div class="event_date">
                    February 14, 2018
                </div>
            </div>
            <div class="event_word">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                    <div class="col-md-6 col-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                    </div>
                </div>
            </div>
            <a href="#" class="readmore_btn">read more</a>
        </div>

    </div>
</section>
@include('layouts.footer')

<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>