<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description" content="Evento -Event Html Template">
    <meta name="keywords" content="Evento , Event , Html, Template">
    <meta name="author" content="ColorLib">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title> <?php echo $judul; ?></title>
    <link rel="icon" type="image/png" href="{{ asset('assets/img/logo.png')}}">
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="{{ asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/et-line.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/ionicons.min.css')}}" rel="stylesheet">
        <!-- Carousel CSS -->
    <link href="{{ asset('assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/owl.theme.default.min.css')}}" rel="stylesheet">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css')}}">
    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/main.css')}}" rel="stylesheet">
</head>