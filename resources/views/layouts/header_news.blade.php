<!--header start here -->
<header class=" navbar fixed-top navbar-expand-md sticky_header">
    <div class="container">
        <a class="navbar-brand logo" href="#">
            <img src="{{asset('assets/img/logo.png')}}" alt="Evento"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="lnr lnr-text-align-right"></span>
        </button>
        <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
            <ul class=" nav navbar-nav menu">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('committee') }}">Committee</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('generalvalue') }}">General Value</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('conference') }}"><font color="#4492c5"><b>Conference</b></font></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('/'.'#events') }}">Events</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('news') }}">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('contacts') }}">Contact</a>
                </li> 
         </ul>
     </div>
 </div>
</header>
<!--header end here-->