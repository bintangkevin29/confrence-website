 <!--get tickets section end-->

 <!--footer start -->
 <footer>
     <div class="container">
         <div class="row justify-content-center">

             <div class="col-12 col-md-4">
                 <div class="footer_box">
                     <div class="footer_header">
                         <h4 class="footer_title">
                             Organized By
                         </h4>
                     </div>
                     <div class="footer_box_body">
                         <!-- <center> -->
                         <a href="#">
                             <img src="{{asset('assets/img/cleander/1_abad.png')}}" alt="instagram">
                         </a>
                         <a href="#">
                             <img src="{{asset('assets/img/cleander/c1.png')}}" alt="instagram">
                         </a>
                         <a href="#">
                             <img src="{{asset('assets/img/cleander/IAITBSUMUT.png')}}" alt="instagram">
                         </a>
                         <a href="#">
                             <img src="{{asset('assets/img/cleander/itb.png')}}" alt="instagram">
                         </a>
                         <a href="#">
                             <img src="{{asset('assets/img/cleander/usu.png')}}" alt="instagram">
                         </a>
                         <!-- </center> -->
                     </div>
                 </div>
             </div>
             <div class="col-12 col-md-4">
                 <div class="footer_box">
                     <div class="footer_header">
                         <h4 class="footer_title">
                             Supported By
                         </h4>
                     </div>
                     <div class="footer_box_body">
                            <a href="#">
                                 <img src="{{asset('assets/img/cleander/Logo_Kementerian_ESDM.gif')}}" alt="instagram">
                             </a>
                            <a href="#">
                                 <img src="{{asset('assets/img/cleander/c1.png')}}" alt="instagram">
                             </a>
                             <a href="#">
                                 <img src="{{asset('assets/img/cleander/ecadin.jpeg')}}" alt="instagram">
                             </a>
                             
                     </div>
                 </div>
             </div>
             <div class="col-12 col-md-4">
                 <div class="footer_box">
                     <div class="footer_header">
                         <h4 class="footer_title">
                             Contact Us
                         </h4>
                     </div>
                     <div class="footer_box_body">
                         <ul class="sosmed">
                             <li><i class="fa fa-whatsapp"></i> (+62) 82296073952</li>
                             <li><i class="fa fa-envelope"></i> info@100tahunitb-iaitbsumut.info</li>
                              <li><i class="fa fa-facebook"></i> @mices2020</li>
                             <li><i class="fa fa-twitter"></i> @mices2020</li>
                             <li><i class="fa fa-instagram"></i> @mices2020</li>
                         </ul>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <div class="copyright_footer">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-md-6 col-12">
                 <p>
                     <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                     &copy;<script>
                         document.write(new Date().getFullYear());
                     </script> | Panitia Menuju 100 Tahun ITB</a>
                     <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                 </p>
             </div>
             <div class="col-12 col-md-6 ">
                 <ul class="footer_menu">
                     <li>
                         <a class="nav-link" href="{{ url('/') }}">Home</a>
                     </li>
                     <li>
                         <a class="nav-link " href="{{ url('committee') }}">Committee</a>
                     </li>
                     <li>
                         <a class="nav-link " href="{{ url('conference') }}">
                             <font color="#4492c5"><b>Conference</b></font>
                         </a>
                     </li>
                     <li>
                         <a class="nav-link " href="{{ url('/'.'#events') }}">Events</a>
                     </li>
                     <li>
                         <a class="nav-link " href="{{ url('news') }}">News</a>
                     </li>
                     <li>
                         <a class="nav-link " href="{{ url('contacts') }}">Contact</a>
                     </li>
                 </ul>
             </div>
         </div>
     </div>
 </div>