<!--header start here -->
<header class="header navbar fixed-top navbar-expand-md">
    <div class="container">
        <a class="navbar-brand logo" href="#">
            <img src="{{asset('assets/img/logo.png')}}" alt="Evento" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="lnr lnr-text-align-right"></span>
        </button>
        <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
            <ul class=" nav navbar-nav menu">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('committee') }}">Committee</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('generalvalue') }}">General Value</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('conference/home') }}">
                        <font color="#4492c5"><b>Conference</b></font>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('expo') }}">
                        <font color="#4492c5"><b>Expo</b></font>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <font color="#EC2A90"><b>Registration</b></font>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="{{url('conference/cpapersub')}}" class="dropdown-item">Conference &ndash; Presenter</a>
                        <div class="dropdown-divider"></div>
                        <a href="{{url('conference/cparticipants')}}" class="dropdown-item">Conference &ndash; Participant</a>
                        <div class="dropdown-divider"></div>
                        <a href="{{url('expo#register')}}" class="dropdown-item">Expo &ndash;  Booth</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('expo') }}">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('/'.'#events') }}">Events</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('news') }}">News</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('contacts') }}">Contact</a>
                </li>  -->
            </ul>
        </div>
    </div>
</header>
<!--header end here-->