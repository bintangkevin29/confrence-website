<div class="breadcrumbs">
    <ul>
        <li><a class="{{ Request::segment(2) === 'home' ? 'tebal' : ''}}" judul="" href="{{ url('conference/home')}}">Home</a>
        <li><a class="{{ Request::segment(2) === 'speakers' ? 'tebal' : ''}}" href="{{ url('conference/speakers')}}">Speakers</a></li>
        <li><a class="{{ Request::segment(2) === 'papers' ? 'tebal' : ''}}" href="{{ url('conference/papers')}}">Call for Papers</a></li>
        <li><a class="{{ Request::segment(2) === 'submission' ? 'tebal' : ''}}" href="{{ url('conference/submission')}}">Submission Guideline</a></li>
        <li><a class="{{ Request::segment(2) === 'conferenceguideline' ? 'tebal' : ''}}" href="{{ url('conference/conferenceguideline')}}">Conference Guideline</a></li>
        <li> <a class="{{ Request::segment(2) === 'cpapersub' ? 'tebal' : ''}}" href="{{ url('conference/cpapersub')}}"> Paper Submission </a> </li>
        <li> <a class="{{ Request::segment(2) === 'register' ? 'tebal' : ''}}" href="{{ url('conference/register')}}">Registration</a> </li>
        <li><a class="{{ Request::segment(2) === 'contacts' ? 'tebal' : ''}}" href="{{ url('conference/contacts')}}">Contacts</a></li>
    </ul>
</div>