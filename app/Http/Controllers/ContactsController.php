<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    function index()
    {
    	$data = DB::table('itb_contact')
    	->select('*')
    	->get();
    	$judul = 'Contacts - 100 Years of ITB';
    	return view('contacts', compact('judul', 'data'));
    }
}
