<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\lain;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Session;


class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index(lain $lain)
    {
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        $committee = DB::table('itb_speakers')
            ->select('*')
            ->get();
        $pricing = DB::table('conf_pricing')
            ->select('*')
            ->get();
        $events = DB::table('event_dates')
            ->select('*')
            ->where('display_in', '=', 1)
            ->orderBy('waktu_event', 'asc')
            ->get();
        // var_dump($events);exit;
        $type = DB::table('conf_speakers_type')
            ->select('*')
            ->get();
        foreach ($about as $dabout) {
        }

        $title = $dabout->short_title . ' &ndash; Official Website';
        return view(
            'conference',
            [
                'judul' => $title,
                'about' => $about,
                'committee' => $committee,
                'events' => $events,
                'pricing' => $pricing,
                'type' => $type,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function speakers()
    {
        $committee = DB::table('itb_speakers')
            ->select('*')
            // ->where('id_type','!=',3)
            ->get();
        $type = DB::table('conf_speakers_type')
            ->select('*')
            // ->where('id','!=',3)
            ->get();
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        foreach ($about as $dabout) {
        }
        $title = 'Speakers &ndash; ' . $dabout->short_title;
        return view('speakers', ['judul' => $title, 'committee' => $committee, 'type' => $type, 'about' => $about]);
    }

    public function papers()
    {
        $papers = DB::table('conf_topics')
            ->select('*')
            ->get();
        $events = DB::table('event_dates')
            ->select('*')
            ->where('display_in', '=', 1)
            ->orderBy('waktu_event', 'asc')
            ->get();
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        $venue = DB::table('conf_venue')
            ->select('*')
            ->where('disp_venue', '=', 1)
            ->get();
        foreach ($about as $dabout) {
        }
        $judul = 'Call for Papers &ndash; ' . $dabout->short_title;
        return view('papers', compact('about', 'judul', 'papers', 'events', 'venue'));
    }

    public function submission()
    {
        $guide = DB::table('conf_guide')
            ->select('*')
            ->get();
        $events = DB::table('event_dates')
            ->select('*')
            ->where('display_in', '=', 1)
            ->orderBy('waktu_event', 'asc')
            ->get();
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        foreach ($about as $dabout) {
        }
        $judul = 'Submission Guideline &ndash; ' . $dabout->short_title;
        return view('guide', compact('about', 'judul', 'guide', 'events'));
    }

    public function conferenceguideline()
    {
        $guide = DB::table('conf_conference_guide')
            ->select('*')
            ->get();
        $events = DB::table('event_dates')
            ->select('*')
            ->where('display_in', '=', 1)
            ->orderBy('waktu_event', 'asc')
            ->get();
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        foreach ($about as $dabout) {
        }
        $judul = 'Conference Guideline &ndash; ' . $dabout->short_title;
        return view('guide', compact('about', 'judul', 'guide', 'events'));
    }

    public function events()
    {
        $title = 'Conference Events - 100 Years of ITB';
        return view('cevents', ['judul' => $title]);
    }

    public function cnews()
    {
        $news = DB::table('itb_news')
            ->join(
                'users',
                function ($join) {
                    $join->on('itb_news.id_author', '=', 'users.id');
                }
            )
            ->select('*')
            ->where('display_in', '=', 2)
            ->orderBy('news_time', 'desc')
            ->paginate(5);
        $judul = 'Conference News - 100 Years of ITB';
        return view('cnews', compact('judul', 'news'));
    }

    public function contacts()
    {
        $data = DB::table('itb_contact')
            ->select('*')
            ->get();
        $about = DB::table('itb_about')
            ->select('*')
            ->get();
        foreach ($about as $dabout) {
        }
        $judul = 'Contacts &ndash; ' . $dabout->short_title;
        return view('ccontacts', compact('judul', 'data', 'about'));
    }

    public function contacts_send(Request $request)
    {
        try {
            Mail::send('email_info', ['pesan' => $request->messages, 'email' => $request->email], function ($message) use ($request) {
                $message->subject($request->subject);
                $message->from('kotaksaran@100tahunitb-iaitbsumut.info', $request->name);
                $message->to('bintangkevin29@gmail.com');
            });
            return back()->with('alert-success', 'Berhasil Kirim Email');
        } catch (Exception $e) {
            return response(['status' => false, 'errors' => $e->getMessage()]);
        }
        Session::flash('success', 'Messages are sent.');

        return view('ccontacts', compact('judul', 'data', 'about'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function download_papers()
    {
        $zip_file = 'papers.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $path = storage_path('docs');
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file) {
            // We're skipping all subfolders
            if (!$file->isDir()) {
                $filePath     = $file->getRealPath();

                // extracting filename with substr/strlen
                $relativePath = 'papers/' . substr($filePath, strlen($path) + 1);

                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return response()->download($zip_file);
    }

    public function expo(lain $lain)
    {
        $judul = 'Expo &ndash; Official Website';
        return view(
            'expo', compact('judul')
        );
    }

    public function expoRegister(){
        return redirect('expo#register');
    }
}
