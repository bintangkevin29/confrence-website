<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class NewsController extends Controller
{
	function index()
	{
		$news = DB::table('itb_news')
		->join('users', function($join)
		{
			$join->on('itb_news.id_author', '=', 'users.id');
		}
	)
		->select('*')
		->where('display_in','=',1)
		->orderBy('news_time', 'desc')
		->paginate(5);
		$judul = 'News - 100 Years of ITB';
		return view('news', compact('judul','news'));
	}

	function detail($id)
	{
		$news = DB::table('itb_news')
		->join('users', function($join)
		{
			$join->on('itb_news.id_author', '=', 'users.id');
		}
	)
		->select('*')
		->where('id_news','=', $id)
		->orderBy('news_time', 'desc')
		->get();
		foreach ($news as $dnews) {
		}
		$judul = ucwords($dnews->news_title).' - 100 Years of ITB';
		return view('news_detail', compact('judul','news'));
	}
}
