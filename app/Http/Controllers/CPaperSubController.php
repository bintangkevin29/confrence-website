<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Anam\Captcha\Captcha;
use Illuminate\Support\Facades\Session;
use Mail;

class CPaperSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $judul = 'Paper Submission &ndash; MICES';
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();

        $tracks = DB::table('conf_tracks')
            ->select('*')
            ->get();

        return view('cpapersub', compact('judul', 'about', 'tracks')); //
    }

    public function participants()
    {
        $about = DB::table('itb_about')
            ->select('*')
            ->where('display_in', '=', 2)
            ->get();
        $judul = 'Participants Registration &ndash; MICES';

        $tracks = DB::table('conf_tracks')
            ->select('*')
            ->get();

        return view('cparticipants', compact('judul', 'about', 'tracks')); //
    }

    public function tes()
    {
        return 'tes';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function addParticipants(Request $request, Captcha $captcha)
    {
        $response = $captcha->check($request);
        $dtracks = '';
        $idea = '';
        if (!$response->isVerified()) {
            Session::flash('danger', 'Please check reCAPTCHA');
            return redirect('/conference/cparticipants');
        } else {
            if ($request->tracks == 'other') {
                $dtracks = 0;
            } else {
                $dtracks = $request->tracks;
            }

            if ($request->idea != '') {
                $idea = $request->idea;
            }


            Session::flash('success', 'Participant succesfully registered.');

            DB::table('conf_participants_non_presenter')->insert(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'affiliation' => $request->affiliation,
                    'phone' => $request->number,
                    'aff_type' => $request->aff_type,
                    'id_tracks' => $dtracks,
                    'idea' => $idea,
                ]
            );
            $id_admin = DB::table('conf_paper_admin')
                ->select('*')
                ->get();
            foreach ($id_admin as $did) {
                $id_adm = $did->id_user;
            }
            $admin = DB::table('users')
                ->where('id', '=', $id_adm)
                ->select('*')
                ->first();

            $namaadmin = $admin->email;
            try {



                Mail::send('email_participants_admin', ['nama' => $request->name], function ($message) use ($namaadmin) {
                    $message->subject('[MICES 2020] New Participant Registered.');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    // $message->to($request->cor_email);
                    $message->to($namaadmin);
                });

                Mail::send('email_participants', ['nama' => $request->name], function ($message) use ($request) {
                    $message->subject('[MICES 2020] Thank you for registering.');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    // $message->to($request->cor_email);
                    $message->to($request->email);
                });

                Mail::send('email_participants', ['nama' => $request->name], function ($message) use ($request) {
                    $message->subject('[MICES 2020] Thank you for registering.');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    // $message->to($request->cor_email);
                    $message->to('fahmimn@gmail.com');
                });

                Mail::send('email_participants', ['nama' => $request->name], function ($message) use ($request) {
                    $message->subject('[MICES 2020] Thank you for registering.');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    // $message->to($request->cor_email);
                    $message->to('zpn.usu@gmail.com');
                });

                return back()->with('alert-success', 'Berhasil Kirim Email');
            } catch (Exception $e) {
                return response(['status' => false, 'errors' => $e->getMessage()]);
            }

            return redirect('/conference/cparticipants');
        }
    }
    public function add(Request $request, Captcha $captcha)
    {
        $response = $captcha->check($request);

        if (!$response->isVerified()) {
            Session::flash('danger', 'Please check reCAPTCHA');
            return redirect('/conference/cpapersub');
        } else {
            $dtracks = DB::table('conf_tracks')
                ->where(
                    'id_tracks',
                    '=',
                    $request->tracks
                )
                ->select('*')
                ->get();

            foreach ($dtracks as $dt) {
                $code = $dt->code;
            }
            Session::flash('success', 'Paper succesfully added.');

            $file = $request->file('file');
            $fileextension = $file->getClientOriginalExtension();
            $uploaded_to = 'storage/docs';
            $filename = $code . '_FP_' . $request->cor_name . '_' . time() . '.' . $fileextension;
            $file->move($uploaded_to, $filename);

            $file2 = $request->file('file2');
            $fileextension2 = $file2->getClientOriginalExtension();
            $uploaded_to2 = 'storage/docs';
            $filename2 = $code . '_ABS_' . $request->cor_name . '_' . time() . '.' . $fileextension2;
            $file2->move($uploaded_to2, $filename2);

            DB::table('conf_participants')->insert(
                [
                    'name' => $request->cor_name,
                    'email' => $request->cor_email,
                    'affiliation' => $request->affiliation,
                    'id_tracks' => $request->tracks,
                    'title' => $request->title,
                    'abstract' => $request->abstract,
                    'author' => $request->author,
                    'file' => $filename,
                    'fileAbstract' => $filename2,
                    'fileAbstract' => $filename2,
                    'published' => $request->published,
                ]
            );
            $id_admin = DB::table('conf_paper_admin')
                ->select('*')
                ->get();
            foreach ($id_admin as $did) {
                $id_adm = $did->id_user;
            }
            $admin = DB::table('users')
                ->where('id', '=', $id_adm)
                ->select('*')
                ->get();

            foreach ($admin as $dad) {
                $namaadmin = $dad->email;
            }



            try {
                Mail::send('email', ['published' => $request->published, 'nama' => $request->cor_name, 'judul' => $request->title], function ($message) use ($request) {
                    $message->subject('[MICES 2020] Submission Acknowledgment');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    $message->to($request->cor_email);
                });

                Mail::send('email_admin', ['nama' => $request->cor_name], function ($message) use ($namaadmin) {
                    $message->subject('New Presenter Registered');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    $message->to($namaadmin);
                });

                Mail::send('email_admin', ['nama' => $request->cor_name], function ($message) use ($namaadmin) {
                    $message->subject('New Presenter Registered');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    $message->to('zpn.usu@gmail.com');
                });

                Mail::send('email_admin', ['nama' => $request->cor_name], function ($message) use ($namaadmin) {
                    $message->subject('New Presenter Registered');
                    $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                    $message->to('fahmimn@gmail.com');
                });
                return back()->with('alert-success', 'Berhasil Kirim Email');
            } catch (Exception $e) {
                return response(['status' => false, 'errors' => $e->getMessage()]);
            }

            return redirect('/conference/cpapersub');
        }
    }

    public function addExpo(Request $request, Captcha $captcha)
    {
        $response = $captcha->check($request);

        if (!$response->isVerified()) {
            Session::flash('danger', 'Please check reCAPTCHA');
            return redirect('/conference/cpapersub');
        } else {

        Session::flash('success', 'Booth registered.');
        DB::table('expo_booth')->insert(
            [
                'regName' => $request->name,
                'regEmail' => $request->email,
                'company' => $request->company,
                'address' => $request->address,
                'isBooth' => $request->booth,
                'isSponsorship' => $request->sponsorship,
                'idCard' => $request->idCard,
                'phone' => $request->phoneNumber,
                'product' => $request->product,
                'price' => $request->price,
            ]
        );
        $id_admin = DB::table('conf_paper_admin')
            ->select('*')
            ->get();
        foreach ($id_admin as $did) {
            $id_adm = $did->id_user;
        }
        $admin = DB::table('users')
            ->where('id', '=', $id_adm)
            ->select('*')
            ->get();

        foreach ($admin as $dad) {
            $namaadmin = $dad->email;
        }



        try {
            Mail::send('email_booth', ['email' => $request->email, 'nama' => $request->name], function ($message) use ($request) {
                $message->subject('[MICES Expo 2020] Registration Acknowledgment');
                $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Expo Committee');
                $message->to($request->email);
            });

            Mail::send('email_expo_admin', ['nama' => $request->company], function ($message) use ($namaadmin) {
                $message->subject('New Booth Registered');
                $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Expo Committee');
                $message->to($namaadmin);
            });

            Mail::send('email_expo_admin', ['nama' => $request->company], function ($message) use ($namaadmin) {
                $message->subject('New Booth Registered');
                $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Expo Committee');
                $message->to('zpn.usu@gmail.com');
            });
            
            Mail::send('email_expo_admin', ['nama' => $request->company], function ($message) use ($namaadmin) {
                $message->subject('New Booth Registered');
                $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Expo Committee');
                $message->to('fahmimn@gmail.com');
            });
 
            return back()->with('alert-success', 'Berhasil Kirim Email');
        } catch (Exception $e) {
            return response(['status' => false, 'errors' => $e->getMessage()]);
        }
        

        return redirect('/expo#register');
    }
    }

    public function tes_kirim(Request $request)
    {

        $id_admin = DB::table('conf_paper_admin')
            ->select('*')
            ->get();
        foreach ($id_admin as $did) {
            $id_adm = $did->id_user;
        }
        $admin = DB::table('users')
            ->where('id', '=', $id_adm)
            ->select('*')
            ->get();

        foreach ($admin as $dad) {
            $namaadmin = $dad->email;
        }
        $kampretos = 'tes';
        try {
            Mail::send('email_admin', ['nama' => $request->cor_name], function ($message) use ($namaadmin) {
                $message->subject('New Participant Registered');
                $message->from('registration@100tahunitb-iaitbsumut.info', 'MICES Committee');
                $message->to($namaadmin);
            });
        } catch (Exception $e) {
            return response(['status' => false, 'errors' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
