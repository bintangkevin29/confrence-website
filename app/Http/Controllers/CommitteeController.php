<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CommitteeController extends Controller
{
    function index(){
    	$committee = DB::table('itb_committee')
        ->select('*')
        ->get();
    	return view('committee',[
    		'judul' => 'Committee - 100 Years ITB',
    		'committee' => $committee
    	]);
    }
}
