<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 1)
        ->get(); 
        $committee = DB::table('itb_committee')
        ->select('*')
        ->get();
        $events = DB::table('event_dates')
        ->select('*')
        ->where('display_in', '=', 1)
        ->orderBy('waktu_event', 'asc')
        ->get();
        $vip = DB::table('itb_vip')
        ->select('*')
        ->get();

        $covertitle = DB::table('covertitle')
        ->select('*')
        ->get();
        
        $title = '100 Years of ITB - Official Website';
        return view('index', [
            'judul' => $title, 
            'about' => $about, 
            'committee' => $committee, 
            'events' => $events, 
            'vip' => $vip,
            'covertitle' => $covertitle
        ]
    );
    }

    public function register()
    {
        $pricing = DB::table('conf_pricing')
        ->select('*')
        ->get();
        $policy = DB::table('conf_policy')
        ->select('*')
        ->get(); 
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get();
        $venue = DB::table('conf_venue')
        ->select('*')
        ->where('disp_venue', '=', 1)
        ->get();  
        
        foreach ($about as $dabout) {

        }
        $judul = 'Regsitration &ndash; '.$dabout->short_title;        
        return view('register', compact('judul','pricing','about','policy','venue'));
    }
}
